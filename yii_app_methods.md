# Yii::app methods

## Request parameters

### Retrieve GET parameter

Let's suppose that we visited page: `http://site.com/?name=johny`. We can get param `name` via this method:

```php
echo Yii::app()->request->getParam('name'); // 'johny'

// Returns null if param doesn't exist
echo Yii::app()->request->getParam('name2'); // null

// We can define default param value
echo Yii::app()->request->getParam('name2', 'kenny'); // 'kenny'
```

### Get URL for the page

```php
Yii::app()->request->getUrl(); // '/open?form=new&design=newaccess&ldg=banks'

Yii::app()->request->getRequestUri(); // '/app/index.php?r=universityHome/view&id=2211'

$params = $this->getActionParams();
$params['format'] = 'pdf';
Yii::app()->createUrl($this->route, $params); // '/app/index.php?r=universityHome/view&id=2211&format=pdf'
```

### Get absolute URL for the page

We can use this at the Yii view:

```php
Yii::app()->createAbsoluteUrl($this->getRoute(), $this->getActionParams());
// Output: "http://127.0.0.1:8090/app/index.php?r=universityHome/view&id=2211"
```