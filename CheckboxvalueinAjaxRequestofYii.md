## Checkbox value in Ajax Request of Yii
>This is one of the way to send data dynamicallay in ajaxbutton request of yii. When you click on ajaxbutton We will get the selected checkbox value and send this via ajax

```php
<?php 
Yii::app()->clientScript->registerScript('checkBoxSelect', "
    function callData()
    {
        fileid= $(\"input[name='usergroup[]']:checked\").map(function() 
        { 
            return this.value; 
        }).get();
        return 'id='+fileid;
    }
");
?>
```
```php
<?php echo CHtml::ajaxButton(
        'User',CController::createUrl('user/active'), 
        array(
            'type' =>'POST',
            'data'=>"js:callData()", //or append data here
            'url'=> 'user/active',
        ),
        array('id'=>'activebtn')
    );    
?>
```