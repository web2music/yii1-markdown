## CHtml Link
CHtml::link() method

* Linking to a controller action
* Linking to a controller action with query string parameters
* Linking to a controller action with multiple query string parameters
* Link opening a new page
* Linking to a controller action inside the actual controller
* Linking to a controller action from the site root
* Linking to a controller action from another module
* Linking to a controller action from the same module
* Linking to a controller action via POST with confirmation dialog
* Linking to a controller action via POST with POST parameters
* Link With Image

>public static string link(string $text, mixed $url='#', array $htmlOptions=array ( ))

Generates a hyperlink tag.

Linking to a controller action

```php
<?php 
echo CHtml::link('Link Text',array('controller/action')); 
?>
```

HTML Output:

```html
<a href="index.php?r=controller/action">Link Text</a>
```
Linking to a controller action with query string parameters

```php
<?php 
echo CHtml::link('Link Text',
            array(
                'controller/action',
                'param1'=>'value1'
            )
        ); 
    ?>
```

HTML Output:

```html
<a href="index.php?r=controller/action&param1=value1">Link Text</a>
```

Linking to a controller action with multiple query string parameters

```php
<?php 
echo CHtml::link('Link Text',
            array(
                'controller/action',
                'param1'=>'value1',
                'param2'=>'value2',
                'param3'=>'value3'
            )
        ); 
    ?>
```    

HTML Output:

```html
<a href="index.php?r=controller/action&param1=value1&param2=value2&param3=value3">
Link Text </a>
```
Link opening a new page

```php
<?php 
echo CHtml::link('Link Text',
            array(
                'controller/action',
                'param1'=>'value1'
            ), 
            array('target'=>'_blank'); 
		);
    ?>
```
HTML Output:

```html
<a target="_blank" href="index.php?r=controller/action&param1=value1">Link Text</a>
```

Linking to a controller action inside the actual controller
(Suppose you are in the PostController/view and wants to link to PostController/create)
Just remove the 'controller' part from the string

```php
<?php 
echo CHtml::link('Link Text',array('action')); 
?>
```

If you are linking to an action from another controller, use the syntax of the
former examples.

Linking to a controller action from the site root
(Suppose you are inside a module and wants to make the link from a controller of the
root application)

In this case, add an slash "/" at the start of the string url

```php
<?php 
echo CHtml::link('Link Text',array('/controller/action')); 
?>
```

This makes more sense if you are working with modules.

Linking to a controller action from another module
Replace below the module-id with desired module id .

```php
<?php echo CHtml::link('Link Text',array('/module-id/controller/action')); ?>
```

## Linking to a controller action from the same module
This is useful when you want to make absolute paths avoiding to use static module names.

```php
<?php 
echo CHtml::link('Link Text',
        array('/{$this->module->id}/controller/action')); 
?>
```
>Linking to a controller action via POST with confirmation dialog
Delete actions created using gii require the delete request be sent via POST to help prevent
deleting objects by accident. Below is an example how to create a link that sends the request
via POST and also asks for confirmation. Where you are redirected after the delete depends
on your delete action. Note that the id link parameter below is a GET type parameter
(submit URL will be something like http://example.com/post/delete/id/100).

```php
<?php 
echo CHtml::link('Delete',"#",
        array(
            "submit"=>array('delete', 'id'=>$data->ID), 
            'confirm' => 'Are you sure?'
        )
    ); 
?>
```
If you are using CSRF protection in your application do not forget to add csrf parameter
to the htmlOptions array.

```php
<?php 
echo CHtml::link('Delete',"#",
        array(
            "submit"=>array('delete', 'id'=>$data->ID),
            'confirm' => 'Are you sure?', 'csrf'=>true
        )
    ); 
?>
```
Linking to a controller action via POST with POST parameters
If you need to make POST request with arbitary link with additional POST parameters you
should use following code (submit URL will be something like http://example.com/blog/deletePost/param/100).
<p>

```php
<?php 
echo CHtml:link('Delete blog post', '#', 
        array(
            'submit'=>array('blog/deletePost', 'param'=>100),
            'params'=>array(
                        'id'=>$post->id, 
                        'status'=>Post::STATUS_DELETED_BY_OWNER
                    ),
            'csrf'=>true,
        )
    ); 
?>
```
</p>

## CHtml::button() method 
Link With Image

```php
<?php 
CHtml::link(
            "<img src='".Yii::app()->baseUrl."/images/userimage.png"."' />",
            Yii::app()->createAbsoluteUrl("user/image",array("id"=>$this->user_id))
        );
?>
```
