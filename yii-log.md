# Yii Log

Yii Log: Logs a message into file. Every developers should know about "log" concept. It will helpful to trace error or application workflow. Yii also have "log" option. It will helpful to built wornderful yii application.
```
Log
Basic Log
New Log File
Yii Trace()

```
## Log

```php
public static void log(string $msg, string $level='info', string $category='application')
```
```
$msg    - message to be logged
$level  - level of the message (e.g. 'trace', 'warning', 'error').
          It is case-insensitive
$category-category of the message (e.g. 'system.web'). It is case-insensitive

```
### Basic Log

Yii have default log system. It is getting the information error, warning and saved this information into
```

"protecte/runtime/application.log" file. When you open it , you will get all error details, warning details etc. Just check you config/main.php file, you will get the "log" configuration details. Basically it have two log types. They are "error","warning" and we can add more log types.
```

```php
### protected/config/main.php

'log'=>array(
    'class'=>'CLogRouter',
        'routes'=>array(
                array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
        ),
    ),

```    
### protected/views/site/index.php

To understand the log system, first clear the "runtime/application.log" file. Now copy the below code and add it into site/index.php file.

```
<?php
Yii::log("Index Error",CLogger::LEVEL_ERROR,'ERROR_CATEGORY');
Yii::log("Index Warning",CLogger::LEVEL_WARNING);
?>
```

### After added about code, you will get the following log details.
Log Message :protected/runtime/application.log
```
2013/06/13 05:26:21 [error] [ERROR_CATEGORY] Index Error
in D:\wamp\www\yiilog\protected\views\site\index.php (22)
in D:\wamp\www\yiilog\protected\controllers\SiteController.php (32)
in D:\wamp\www\yiilog\index.php (12)
2013/06/13 05:26:21 [warning] [application] Index Warning
in D:\wamp\www\yiilog\protected\views\site\index.php (23)
in D:\wamp\www\yiilog\protected\controllers\SiteController.php (32)
in D:\wamp\www\yiilog\index.php (12)
```
### New Log File
Every log details will add into runtime/application.log file. If we want, we can create new log file under "runtime" directory. Now i created one new log file "MailLog". It contains the "mailerror","mailsuccess","mailwarning" log types for mail. I added sourcecode for this concept.
```
'log'=>array(
    'class'=>'CLogRouter',
    'routes'=>array(
                array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, trace',
				),
                // Add New log System
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'mailerror, mailsuccess, mailwarning',
                    'categories'=>"system.*",
                    'logFile'=>'MailLog.log',                                                            
                ),
        ),                
    ),
```    
> protected/views/site/index

```
.......
if($mail->save()) {
      Yii::log("Mail Sent Success",'mailsuccess','system.*');
}else {
      Yii::log("Mail Sent Failed",'mailerror','system.*');
}
.......
```

### Log Message :protected/runtime/MailLog.log
```
2013/06/13 05:38:34 [mailsuccess] [system.*] Mail Sent Success
in D:\wamp\www\yiilog\protected\views\site\index.php (33)
in D:\wamp\www\yiilog\protected\controllers\SiteController.php (32)
in D:\wamp\www\yiilog\index.php (12)
```

### Yii Trace()
Yii::trace(string $msg, string $category='application');
Yii Trace() method will only log a message when the application is in debug mode. See the below code to understand it.
```php
'log'=>array(
    'class'=>'CLogRouter',
        'routes'=>array(
                array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, trace',
				),				
        ),
    ),
```    
> protected/views/site/index

```php
$TRACE_ARRAY=array('ERROR','WARNING');
Yii::trace(CVarDumper::dumpAsString($TRACE_ARRAY),'vardump');
Yii::trace('Trace Message');
```

> Log Message :protected/runtime/MailLog.log

```
............  
2013/06/13 06:22:42 [trace] [vardump] array
(
    0 => 'ERROR'
    1 => 'WARNING'
)

```


```
............
2013/06/13 05:49:20 [trace] [application] Trace Message
............

```
