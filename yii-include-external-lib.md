# How can I call these library functions from anywhere in my Yii app? 
I have a library:

There are several ways.

>1.Register libraries' autoloader:

```php
// Enable Zend autoloader
spl_autoload_unregister(array('YiiBase', 'autoload')); // Disable Yii autoloader

Yii::import('site.common.lib.*'); // Add Zend library to include_path
Yii::import('site.common.lib.Zend.Loader.Autoloader', true); // Require Zend autoloader
spl_autoload_register(array('Zend_Loader_Autoloader', 'autoload')); // Register Zend autoloader
spl_autoload_register(array('YiiBase', 'autoload')); // Register Yii autoloader
```
>2.Add library to the import section in your config/main.php:

```php
return array(           
    // Autoloading
    'import' => array(
        'application.lib.*',
        'application.components.*',
        'site.common.extentions.YiiMongoDbSuite.*',
    ),
);
```
>3.Autoloading anywhere in your application:
```php
Yii::import('application.lib.*');
```

# Other 1

>Place your library in the vendors folder (under protected folder) supposing (all your classes are in MyLib folder) you do like this:

```php
Yii::import('application.vendors.MyLib.*');
```

# Other 2

## I use Yii's own autoloader;

```php
    //include auto loader class of vendor
    require dirname(__FILE__).'/mollie-api-php/src/Mollie/API/Autoloader.php';
    //Now register vendor autoloader class to Yii autoloader 
    Yii::registerAutoloader(array('Mollie_API_Autoloader','autoload'));
```

# Other 3
I have replaced
```php
Yii::setPathOfAlias('Cmfcmf',Yii::getPathOfAlias('application.vendors.Cmfcmf'));
```
with

```php
Yii::setPathOfAlias('Cmfcmf',dirname(__FILE__) . '/../vendors/Cmfcmf');
```
and that solved the problem.
