# Role Based Access Control For Action
>I added the source code for RBAC action to validate the permission. We can check the access role in module level, controller level and action level

>add this in config/main.php
```php
<?php
'authManager'=>array(
    'class'=>'CDbAuthManager',
    'itemTable'=>'authitem',
    'assignmentTable'=>'authassignment',
    'itemChildTable'=>'authitemchild',
    'connectionID'=>'db',  
),
?>
```
>In controller page
```php
class YiicontrolController extends Controller
{
    protected function beforeAction($action)
    {
        if(Yii::app()->user->checkAccess($this->getAction()->getId().'Post'))
        { 
            return true;
        }else 
        {
            Yii::app()->request->redirect(Yii::app()->user->returnUrl);
        }
    }
}

```
