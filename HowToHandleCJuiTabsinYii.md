# How To Handle CJuiTabs in Yii


I have created this yii cjuitabs article from my experience.When you read this article you can understand the yii cuitabs to handle differenct way.When i work on project, I need to assign the color for each tabs. I did this using span.


* Static CJuiTabs
* Render CJuiTabs
* Dynamic CJuiTabs
* CJuiTabs With Class(Style)
* Dynamic Yii CJui Tabs Menu With Color
* Yii CJui Tabs Mouse Over Event
* Default Selected CJuiTabs
* Default Selected CJuiTabs Using Session
* Static CJuiTabs

```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => array(
        'Tab 1' => 'Content for tab 1',
        'Tab 2' => array('content' => 'Content for tab 2', 'id' => 'tab2'),
        // panel 3 contains the content rendered by a partial view
        'AjaxTab' => array('ajax' => $this->createUrl('...')),
    ),
    // additional javascript options for the tabs plugin
    'options' => array(
        //Click the selected tab to toggle its content closed/open.
        //To enable this functionality, set the collapsible option to true
        'collapsible' => true,

        //Open CJuitabs on mouse over
        'event'=>'mouseover',   
    ),
));
?>
```
This is the normal CJuiTabs view in Yii framework. Just create the tab and assign content for that tab.

Here I did 3 things. They are

* Static Content  for Tab l
* Static Content with ID for Tab 2
* Content added dynamically using ajax url

Render CJuiTabs
```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => array(
        'Tab 1' => 'Content for tab 1',
        'Tab 2' => array('content' => 'Content for tab 2', 'id' => 'tab2'),   
        // panel 3 contains the content rendered by a partial view
        'AjaxTab' => array('ajax' => $this->createUrl('...')),  
        // Get Content From Another page. Also Pass Parameter
        'Render View'=>$this->renderPartial(
                '_newpage',
                array('value'=>$value),TRUE)
    ),
	
    // Render view with ID
    'Render View With ID'=>array(
            'id'=>'renderid'
            'content'=>$this->renderPartial('_newpage2',
                array('value'=>$value),TRUE
            ),
    ),
	
    // additional javascript options for the tabs plugin
    'options' => array(
        'collapsible' => true,
    ),
  
    'id'=>'MyTab',
));
?>
```

In this code, We can add content using renderPartial method in CJuiTabs. Here i added one parameter that is "value"(Optional)

>Dynamic CJuiTabs

```php
<?php
$tab_list=Componenttabs::gettabs();
$tabarray=array();
$i=1;

// Create Dynamic Tabs 
foreach($tab_list as $key=>$value)
{
    $tabarray["Tab $i"]=array(
        'id'=>$i,
        'content'=>$this->renderPartial('_newpage',
        array('value'=>$value),TRUE)
    );
    $i++;
}

$this->widget('zii.widgets.jui.CJuiTabs',
    array( 
        'tabs'=>$tabarray,
        'options'=>array(
            'collapsible'=>true,
        ),
        'id'=>'categorytabs',
    )
);
?>
```

>components/Componenttabs.php

```php
<?php
class Componenttabs extends CApplicationComponent
{
    public static function gettabs()
    {
        $model=Category::model()->findAll();
        $listdata=CHtml::listData($model,”,’name’);
        return $listdata;
    }
}
?>
```
This is my great work of my project. I created dynamic tabs and content.

>CJuiTabs With Class(Style)

```css
<style type='text/css' >
.tabclass{
    color:'red';
    font-weight:bold; 
}
</style> 
```
// Css class for dynamic CJuiTabs
```php
<?php 
$tabarray["<span class='tabclass'>Tab $i</span>"]=array(
            'id'=>$i, 
            'content'=>$this->renderPartial(
                '_newpage',
                array('value'=>$value),
               TRUE
            )
        );
?>
```

// Css class for static CJuiTabs
```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => array(
            '<span class='tabclass'>Tab 1</span>' => 'Content for tab 1',
    ),
));
?>
```

When you create dynamic tab using $tabarray["Tab $i"], You can apply color class using span tag.

Hint: When you use more than one CJuiTabs "Dont Forget" to set "ID". Otherwise you will get some problem

Dynamic Yii CJui Tabs Menu With Color

```php
<?php
$tablist=array("Red","Green","Blue");
foreach($tablist as $tabs)
{
    $css='';
    if($tabs=='Red'){
	    $css='color:red;';
    }else if($tabs=='Green'){
        $css='color:green;';
    }else if($tabs=='Blue'){
        $css='color:blue';
    } 
    $tabarray["<span id='tab-$key' style='$css'>$tabs</span>"]="$tabs Color";
}
?>

<?php
$this->widget('zii.widgets.jui.CJuiTabs',array(
    'tabs'=>$tabarray,
    'options' => array(
        'collapsible' => true,        
    ),
    'id'=>'MyTab-Menu1'
));
?>
```

Yii CJui Tabs Mouse Over Event

```php
<?php
$tablist=array("Red","Green","Blue");
foreach($tablist as $tabs)
{
    $css='';
    if($tabs=='Red'){
	    $css='color:red;';
	}else if($tabs=='Green'){
	    $css='color:green;';
	}else if($tabs=='Blue'){
	    $css='color:blue';
    } 
    $tabarray["<span id='tab-$key' style='$css'>$tabs</span>"]="$tabs Color";
}
?>

<?php
$this->widget('zii.widgets.jui.CJuiTabs',array(
    'tabs'=>$tabarray,
    'options' => array(         
        'event'=>'mouseover',
    ),
    'id'=>'MyTab-Menu-Mouse'
));
?>
```

>Default Selected CJuiTabs

This section will help you to learn about default selection of cjuitabs from our tab list. For this, we have to get and assign the tab number to 'selected' parameter. Please look the below example
I have two tabs. When i submit tab2 form, the page will get refresh. After refreshing my page still show the tab2 content. For this i used the 'tab number' in hidden field.It will start with 0,1,2 etc.

>Tabview.php

```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => array(
        'Tab 1'=>$this->renderPartial('tab1', array('value'=>$value),TRUE),
        'Tab 2'=>$this->renderPartial('tab2', array('value'=>$value),TRUE)    
        ),
		
    // additional javascript options for the tabs plugin
    'options' => array(
        'collapsible' => true,
        'selected'=>isset($_GET['tab_id']) && is_numeric($_GET['tab_id'])?$_GET['tab_id']:0
    ),
  
    'id'=>'MyTab',
));
?>
```

>tab1.php

```php
<?php
echo 'THIS IS TAB 1 FORM';
?>
```
```html
<form method='GET'>
    <input type='hidden' name='tab_id' value='0' />
    <input type='submit' value='Tab 1 Submit' />
</form>
```

>tab2.php

```php
<?php
echo 'THIS IS TAB 2 FORM';
?>
```

```html
<form method='GET'>
    <input type='hidden' name='tab_id' value='1' />
    <input type='submit' value='Tab 2 Submit' />
</form>
```

>Default Selected CJuiTabs Using Session

Even If you refresh the page, CJuiTabs still show your current active tab.
Add the below code in your file to test the output. Please look the 'select' property inside cjuitabs options, i added the ajax request code in that options. Using session value of 'tabid', i showed the last selected tabs even we refresh the page.

Add Ajax Request Function In CJuiTabs

>viewfile.php

```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => array(
        'Tab 1'=>$this->renderPartial('tab1', array('value'=>$value),TRUE),
        'Tab 2'=>$this->renderPartial('tab2', array('value'=>$value),TRUE) 
    ),

    // additional javascript options for the tabs plugin
    'options' => array(
        'collapsible' => true,
        'selected'=>isset(Yii::app()->session['tabid'])?Yii::app()->session['tabid']:0,
        'select'=>'js:function(event, ui){
            var index=ui.index;
            $.ajax({
                "url":"'.Yii::app()->createUrl('site/tabidsession').'",
                "data":"tab="+index,
            });
        }',
    ),
    'id'=>'MyTab',
));
?>
```
>Set Tab ID In Session

When we click on the cjui tabs, it will trigger the ajax request to set the active tab id. Please see the below code to set the tab id into session and add this code into your controller and configure the controller, action name in cjuitabs select property.

>controller.php

```php
<?php
public function actionTabidsession()
{
    Yii::app()->session['tabid']=isset($_GET['tab']) && is_numeric($_GET['tab'])?$_GET['tab']:0;
    return;
}
?>
```