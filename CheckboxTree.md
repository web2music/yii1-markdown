# Checkbox Tree

```php
<?php
    $data=array(
        '1'=>array('parentid'=>'','text'=>'One'),
        '2'=>array('parentid'=>'','text'=>'Two'),
        '3'=>array('parentid'=>'','text'=>'Three'),
        '11'=>array('parentid'=>'1','text'=>'One-One'),
        '12'=>array('parentid'=>'1','text'=>'One-Two'),        
    );
    Yii::import("application.extensions.AIOTree.*");
    $this->Widget('AIOTree',array(
        //'model'=>$model,
        //'attribute'=>'category',    
        'data'=>$data,
        'type'=>'checkbox',
        'parentShow'=>true,
        'parentTag'=>'div',
        'parentId'=>'aiotree_id',
        
        'selectParent'=>true,
        
        
        'controlTag'=>'div',
        //'controlClass'=>'CC',
        //'controlId'=>'CId',
        //'controlStyle'=>'color:red;',
        'controlDivider'=>' <=> ',
        'controlLabel'=>array('collapse'=>' COLLAPSE '),
        'controlHtmlOptions'=>array(
                            'id'=>'control_id',
                            'class'=>'control_class',
                            'style'=>'color:blue;',
                        ),
        'liHtmlOptions'=>array(
                       //    'class'=>'link-class',                            
            ),                                
    ));
?>
```