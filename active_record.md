# CActiveRecord methods

## findAll()

### Sort records

```php
// Ascending order
Partner::model()->findAll(array('order' => 'company'));

// Descending order
Partner::model()->findAll(array('order' => 'company DESC'));
```

### Find with condition

```php
$criteria = new CDbCriteria();
$criteria->addCondition('idpartner = :partnerId');
$criteria->params[':partnerId'] = 123;
Partner::model()->findAll($criteria);
```

### Select only one column

```php
// Example 1
$partners = Partner::model()->findAll(array('select' => 'title'));

// Example 2
$criteria = new CDbCriteria();
$criteria->select = 'title';
$partners = Partner::model()->findAll($criteria);
```

## findAllBySql()

This method finds ActiveRecord entities by raw SQL:

```php
$sql = "SELECT * FROM products WHERE created_at > :created_at";

$products = Product::model()->findAllBySql($sql, [
  ':created_at' => '2019-01-11 00:00:00',
]);
```

### Insert list of values into SQL query

We didn't find a way to do this in more beautiful way. This just works:

```sql
$statuses = [1, 2];
$statusesString = "'" . implode("', '", $statuses) . "'";

$sql = "SELECT * FROM products WHERE 
    status IN ({$statusesString})";
echo $sql; // SELECT * FROM products WHERE status IN ('1', '2')
$products = Products::model()->findAllBySql($sql);
```

## exists()

Method returns `true` if record was found with defined search criteria.

```php
$criteria = new CDbCriteria();
$criteria->addCondition('product_id = '. $this->productOpenId);
$result = ClaimsIndeces::model()->exists($criteria); // true
```

## updateAll()

We can use this method to update several records that meet some condition:

```php
$criteria = new CDbCriteria();
$criteria->addInCondition('id', [1, 5]);

// This query will update `status` field to `sold` for record's ID = 1, 5
Product::model()->updateAll(
    ['status' => 'sold'],
    $criteria
);
```

## updateByPk()

Method updates defined fields for one record with defined ID.

```php
$res = Claims::model()->updateByPk(123, ['add_status_info' => 'Pending']);
var_dump($res); // 1
```

## save()

Method allows create or update record.

```php
// Example 1
$product = new Product();
$product->title = 'PHP Secrets';
$product->description = 'Latest secrets about php';
$product->save();

// Example 2
$product = new Product();
$product->attributes = [
  'title' => 'PHP Secrets',
  'description' => 'Latest secrets about php'
];
$product->save();
```

## findByAttributes

```php
$claimsInfo = ClaimsInfo::model()->findAllByAttributes(['mobile' => '79110001155']);
var_dump($claimsInfo); // object(ClaimsInfo)[934] ...
```

## findAllByAttributes

```php
$claimsInfo = ClaimsInfo::model()->findAllByAttributes(['mobile' => '79110001155']);
var_dump($claimsInfo); // array (size=11) ...
```

## deleteAll

We can delete records of some model with limit and condition:

```php
$criteria = new CDbCriteria();
$criteria->limit = 2;
$criteria->addCondition('create_time < :createTime');
$criteria->params[':createTime'] = '2018-04-22 00:00:00';

$apiLogDeleted = ApiLog::model()->deleteAll($criteria);
var_dump($apiLogDeleted); // (int) 2
```

## join

```php
$criteria = new CDbCriteria();
$criteria->addCondition('t.mobile = :mobile');

$criteria->join = 'LEFT JOIN claims c ON c.id = t.claim_id';

$criteria->addColumnCondition(['c.design_id' => 29]);
$criteria->limit = 1;

$criteria->params[':mobile'] = $mobile;

$claimIndex = ClaimsIndeces::model()->find($criteria);
```

It will execute SQL query:

```sql
SELECT `t`.`id`, `t`.`claim_id`, `t`.`product_id`, `t`.`form_id`
FROM `claims_indeces` `t`
LEFT JOIN claims c ON c.id = t.claim_id
WHERE 
	(t.mobile = '79221111111') 
	AND (c.design_id='29')
LIMIT 1
```

# Query Builder

The Yii Query Builder provides an object-oriented way of writing SQL statements. It allows developers to use class methods and properties to specify individual parts of a SQL statement.

See [documentation](https://www.yiiframework.com/doc/guide/1.1/en/database.query-builder).

## delete()

```php
$command = Yii::app()->db->createCommand();
$claim_ids = [545, 546];
$affectedRows = $command->delete('claims_calls', ['in', 'claim_id', $claim_ids] );
var_dump($affectedRows); // (int) 2

```
This code will execute SQL:

```sql
DELETE FROM `claims_calls` WHERE `claim_id` IN (545, 546)
```

## Execute raw SQL query

### Without data binding

```php
$sql = "select * from products";
$rows = Yii::app()->db->createCommand($sql)->queryAll();

foreach($rows as $row) {
    echo $row['title'];
    // ...
}
```
### With data binding

One value to bind:
```php
$products = Yii::app()->db->createCommand('SELECT * FROM products WHERE status = :status')
    ->bindValue(':status', $status)
    ->queryAll();
```

Many values to bind:
```php
$sql = 'SELECT * FROM products WHERE status = :status AND created_at > :created_at';
$products = Yii::app()->db->createCommand($sql)
    ->bindValues([
        ':status' => $status,
        ':created_at' => '2017-01-01'
    ])
    ->queryAll();
```

## Execute raw DELETE query

To get a number of deleted rows we can use `execute()`:

```php
$sql = "DELETE FROM api_log WHERE create_time < :createTime LIMIT 2";
$command = Yii::app()->db->createCommand($sql)
    ->bindValues([
        ':createTime' => '2018-04-22 00:00:00'
    ]);
$affectedRows = $command->execute();
var_dump($affectedRows); // (int) 2
```

## Get text of SQL query

Suppose we want to see SQL generated by this expression:

```php
$criteria = new CDbCriteria();
$criteria->addCondition('t.product_id = :productId');
$criteria->params = [':productId' => 1];
$result = ClaimsIndeces::model()->findAll($criteria);
```

We can do this via this service method, that can be placed at `protected/components/Services.php`:

```php
class Services
    // ...
    
    /**
     * Method returns text of SQL-query
     * @param CActiveRecord $model
     * @param CDbCriteria $criteria
     * @return string
     * Example:
     *      $criteria = new CDbCriteria();
     *      $criteria->addCondition('t.product_id = :productId');
     *      $criteria->params = [':productId' => 1];
     *      $result = ClaimsIndeces::model()->findAll($criteria);
     *
     *      echo Services::getSQL(ClaimsIndeces::model(), $criteria);
     *      // Returns: SELECT * FROM `claims_indeces` `t` WHERE t.product_id = 1
     */
    public static function getSQL($model, $criteria)
    {
        if (is_null($criteria->params)) {
            return null;
        }

        $_criteria = $model->getCommandBuilder()->createCriteria($criteria);
        $command = $model->getCommandBuilder()->createFindCommand($model->getTableSchema(), $_criteria);

        $sql = $command->getText();

        krsort($criteria->params);

        foreach ($criteria->params as $key => $value) {
            $sql = str_replace($key, "'$value'", $sql);
        }

        return $sql;
    }
}
```

## Queries with join

Yii doc: [query builder](http://www.yiiframework.com/doc/guide/1.1/en/database.query-builder)

```php
$result = Yii::app()->db->createCommand()
  ->select('pps.*')
  ->from('products_point_sales pps')
  ->join('products_to_point_sales ptps', 'ptps.point_sales_id = pps.sap_id')
  ->join('products p', 'p.crm_id = ptps.product_id')
  ->where('p.alias = :alias', [':alias' => 'open'])
  // ->text  - this will show SQL query
  ->queryAll();
```
It returns array:
```php
array (size=2)
  0 => 
    array (size=6)
      'id' => string '427' (length=3)
      'sap_id' => string '3' (length=1)
      'name' => string 'Первый' (length=27)
      'mvz' => string '6210' (length=4)
      'open_date' => string '0000-00-00 00:00:00' (length=19)
      'close_date' => string '0000-00-00 00:00:00' (length=19)
  1 => 
    array (size=6)
      'id' => string '1822' (length=4)
      'sap_id' => string '5' (length=1)
      'name' => string 'Второй' (length=33)
      'mvz' => string '6211' (length=4)
      'open_date' => string '0000-00-00 00:00:00' (length=19)
      'close_date' => string '0000-00-00 00:00:00' (length=19)
```

## Query with distinct

```php
$result = Yii::app()->db->createCommand()
    ->selectDistinct('code')
    ->from('gsm_operators')
    ->queryAll();
```

It returns query:

```php
array (size=79)
  0 => 
    array (size=1)
      'code' => string '900' (length=3)
  1 => 
    array (size=1)
      'code' => string '901' (length=3)
```

# Related models

## Update attribute of related model

```php
$claim = Claims::model()->findByPk(100);
$claim->info->email = 'hello@gmail.com';
$claim->info->save(); // New email will be saved
// $claim->save(); // Note: this won't save email in related model
```

# Additional Methods for ActiveRecord

We can add new methods to native Yii CActiveRecord class by creating new class `ActiveRecord` in `protected/components/ActiveRecord.php`. To make new methods available in our models we should extend their classes from our custom `ActiveRecord` class:

```php
class Car extends ActiveRecord {
    // ...
}
```

## findOrInitialize()

```php
class ActiveRecord extends CActiveRecord {
	/**
	* This method find record by attributes in database or initialize new one otherwise
	* @param array $attributes
	* @return array|mixed|null
	*/
	public function findOrInitialize($attributes=array())
	{
		$object = $this->findByAttributes($attributes);

		if (!is_null($object)) {
			return $object;
		}

		$modelClassName = get_class($this);

		$object = new $modelClassName;
		$object->setAttributes($attributes, false);
		return $object;
	}
}
```
Usage:
```php
$car = Car::model()->findOrInitialize(array(
	'model' => 'audi',
	'year' => 2017
));
```