## Yii 1.0 Form Label Configuration
> This tutorial will help your to add the span, class, style configuration dynamically for the Yii Form Label (Yii Framework 1.x). We can give ajax parameter using the below code.

> It will helpful for dynamic form requirement and validation.

Form Configuration

```php
<?php
$group_list=array();
$ajax=array();
ajax['type']='GET';
$ajax['url']=Yii::app()->createAbsoluteUrl('ajax/grouplist');
$ajax['update']='#'.CHtml::activeId($model,'unit');
$ajax['data']=array('id'=>'js:this.value');
$ajax['onchange']="js:resetunit()";
	
$group_ajax['ajax']=$ajax;
$group_ajax['empty']='Select Group';
$group_ajax['class']='form-control';
//$group_ajax['disabled']="disabled"; to disable the dropdownlist
$group_ajax['style']="display:none";
	
$group_options['lable_1']=array(
            'class'=>'control-label col-sm-2',
            'label'=>$model->getAttributeLabel('group').' <span class="required">*</span>');
?>
<div class="form-group" >
<?php echo $form->labelEx($model,'group',$group_options['lable_1']); ?>
    <div class="col-sm-10">
        <?php echo $form->dropDownList($model,'group',$group_list,$group_ajax); ?>
        <?php echo $form->error($model,'group'); ?>
    </div>
</div>
```
## Form HTML And Javascript Output

```html
<div class="form-group">
    <label for="Employee_group" class="control-label col-sm-2 required">
        Group <span class="required">*</span>
    </label>
    <div class="col-sm-10">
        <select id="Employee_group"
            name="Employee[group]"
            style="display:block"
            disabled="disabled"
            class="form-control">
                <option value="">Select Group</option>
                <option value="1">Group 1</option>
                <option value="2">Group 2</option>
        </select>
    </div>
</div>
Ajax Script
<script type="text/javascript">
/*<![CDATA[*/
jQuery(function($) {
    jQuery('body').on('change','#Employee_group',function(){
        jQuery.ajax({
            'type':'GET',
            'url':'http://127.0.0.1/test/index.php?r=ajax/grouplist',
            'data':{
                'id':this.value,
            },
            'onchange':resetcluster(),
            'cache':false,
            'success':function(html){
                jQuery("#Employee_unit").html(html)}
            });
            return false; 
    });
});
/*]]>*/
</script>
```