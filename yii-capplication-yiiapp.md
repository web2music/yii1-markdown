# Yii CApplication ->Yii::app()

CApplication is the base class for all application classes. It contains global variables, functions etc. Here i added some most reused functions and properties of yii capplication. Simply we will call this "Yii::app()"
Properties/ Functions	Output
```
Yii::app()->name	My Web Application
Yii::app()->basePath	D:\wamp\www\yiiapi\protected
echo Yii::app()->baseUrl	/yiiapi
Yii::app()->homeUrl	/yiiapi/index.php
Yii::app()->id	3fe52c6c
Yii::app()->language	en_us
Yii::app()->timeZone	UTC
Yii::app()->localeDataPath	D:\wamp\www\yiiapi\yiiframework\framework\i18n\data
Yii::app()->createAbsoluteUrl("/")	http://127.0.0.1/yiiapi/index.php
Yii::app()->createUrl("/")	/yiiapi/index.php
Yii::app()->getBasePath()	D:\wamp\www\yiiapi\protected
Yii::app()->getBaseUrl()	/yiiapi
Yii::app()->getHomeUrl()	/yiiapi/index.php
Yii::app()->getId()	3fe52c6c
Yii::app()->getLanguage()	en_us
Yii::app()->getTimeZone()	UTC
Yii::app()->setBasePath()
Yii::app()->setHomeUrl()
Yii::app()->setId()
Yii::app()->setLanguage()
Yii::app()->SetTimeZone()	Asia/Calcutta

```
