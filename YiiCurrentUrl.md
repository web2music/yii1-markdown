## Yii Current Url

* To Get Current Url
* Get Action Name
* Get Controller Name

To Get Current Url
```php
echo Yii::app()->request->requestUri;
```
Get Action Name
```php
//Anywhere
Yii::app()->controller->action->id
//Inside Controller
public function beforeAction($action)
{
    $actionId = $action->id;
    ..........
}
```

Get Controller Name
```php
// Anywhere
Yii::app()->controller->id 
//Inside Controller
echo $this->getId();
```
