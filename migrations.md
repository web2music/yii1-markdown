# Yii Migrations

## Yii migration column types

- `pk`: an auto-incremental primary key type, will be converted into “int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY”
- `string`: will be converted into `varchar(255)`
- `text`: will be converted into `text`
- `integer`: will be converted into `int(11)`
- `boolean`: will be converted into `tinyint(1)`
- `float`: will be converted into `float`
- `decimal`: will be converted into `decimal`
- `datetime`: will be converted into `datetime`
- `timestamp`: will be converted into `timestamp`
- `time`: will be converted into `time`
- `date`: will be converted into `date`
- `binary`: will be converted into `blob`

## Examples of migration commands

Contents of migration `protected\migrations\m190109_052549_create_claims_autoinform_individuals.php`:

```php
class m190109_052549_create_claims_autoinform_individuals extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable(
            'claims_autoinform_individuals',
            [
                'id' => 'pk',
                'claim_id' => 'int(11) UNSIGNED UNIQUE NOT NULL',
                'settings_autoinformator_id' => 'integer NOT NULL',
                'status' => 'int(1) NOT NULL',

                'create_time' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
                'update_time' => 'timestamp NOT NULL',
            ]
        );

        $this->createIndex('status', 'claims_autoinform_individuals', 'status');

        $this->addForeignKey(
            'fk_claims_autoinform_individuals_claim_id',
            'claims_autoinform_individuals', 'claim_id',
            'claims', 'id'
        );

        $this->addForeignKey(
            'fk_claims_autoinform_individuals_settings_autoinformator_id',
            'claims_autoinform_individuals', 'settings_autoinformator_id',
            'settings_autoinformator', 'id'
        );

    }

	public function safeDown()
	{
	    $this->dropForeignKey('fk_claims_autoinform_individuals_claim_id', 'claims_autoinform_individuals');

	    $this->dropForeignKey(
	        'fk_claims_autoinform_individuals_settings_autoinformator_id',
            'claims_autoinform_individuals'
        );

	    $this->dropTable('claims_autoinform_individuals');
	}
}
```