# Automated display of flash messages  
> Posted on October 4th, 2010 in PHP, Yii | No Comments ?
Step 1. Put this method into your WebUser class:

```php
public function getFlashKeys ( ) 
{ 
    $counters=$this->getState(self::FLASH_COUNTERS); 
        if(empty($counters)) 
            return array(); 
        return array_keys($counters); 
}

``` 

## Step 2. Put this piece of code into your layout or view where a flash message would be appropriate:

```php
<?php 
$user=Yii::app()->user(); 
foreach($user->getFlashKeys() as $key): 
    if($user->hasFlash($key)): ?> 
        <div class="flash-<?php echo $key; ?>"> 
            <?php echo $user->getFlash($key); ?> 
        </div> 
<?php 
    endif; 
endforeach; 
?>
 

```
## Step 3. Put lines of code similar to these where appropriate to notify the user of the corresponding event types:

```php
Yii::app()->user()->setFlash('error','Could not save data to the database.'); 
Yii::app()->user()->setFlash('notice','There was nothing to save.'); 
Yii::app()->user()->setFlash('success','The data was successfully saved to the database.');
```

## Step 4. Put these lines of code your css file

```php
div.flash-error, div.flash-notice, div.flash-success
{ padding:.8em; margin-bottom:1em; border:2px solid #ddd; }
div.flash-error{ background:#FBE3E4; color:#8a1f11; border-color:#FBC2C4; }
div.flash-notice { background:#FFF6BF; color:#514721; border-color:#FFD324; }
div.flash-success { background:#E6EFC2; color:#264409; border-color:#C6D880; }
div.flash-error a { color:#8a1f11; }
div.flash-notice a { color:#514721; }
div.flash-success a { color:#264409; }

```