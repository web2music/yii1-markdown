# CKEditor In Yii Framework

Download And Install CKEditor
CKEditor With Yii
Download And Install CKEditor
CKEditor will helpful to create pages with large content. Normally we will use it "CMS" type of website, pages like "About Us", "Terms and privacy", etc . First we have to download the CKEditor using below url http://ckeditor.com/download After downloaded it, We have to integrate it into our project. Extract the downloaded file. Copy and paste the ckeditor folder outside protected folder. It will place like this

```
project/ckeditor/...
project/protected/...
```

### CKEditor With Yii
Form WithOut CKEditor

yii-ckeditor-1
```css
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content'); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
```
# Form With CKEditor

yii-ckeditor-2
```css
<?php
$js=Yii::app()->getClientScript();
$js->registerScriptFile(Yii::app()->baseUrl.'/ckeditor/ckeditor.js');
$js->registerScriptFile(Yii::app()->baseUrl.'/ckeditor/adapters/jquery.js');

$js->registerScript(
  'js2',
  '
    var config = {
    toolbar:
    [
     ["Bold", "Italic","Underline", "-", "NumberedList", "BulletedList", "-" ],  
     ["UIColor"],["TextColor"],["Undo","Redo","Link"],
     ["JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
     ["NumberedList","BulletedList","FontSize","Font","Preview"]
    ],
    height:150,
    width:580
    };
    $("#page_content").ckeditor(config);
  ',
  CClientScript::POS_LOAD
);
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content'); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
```
