## CHtml Dropdownlist
>Using CHtml class we are creating dropDownList in yii framework. This post will help you to understand about how to create dropDownList in different method, How to assign value, how to get dropDownList value in controller, dropDownList value from database, dropDownList validation in model yii framework.


* Default Empty
* DropDownList
* DropDownList Value In Controller
* DropDownList Event
* Default Empty

>Empty string, specifies the text corresponding to empty selection. Its value is empty. The 'empty' option can also be an array of value-label pairs. Each pair will be used to render a list option at the beginning. If you set required in model and selected empty value from dropdownlist, You will get "required" error on form.

## DropDownList
Using below code, we can create dropDownList in different way. And i added dropDownList value from database and assigned static array.

activeDropDownList With CHtml
CHtml activeDropDownList generates a dropDownList for a model attribute in yii framework.

## Syntax:

```php
public static string activeDropDownList(CModel $model,
                    string $attribute, 
                    array $data, 
                    array $htmlOptions=array ( )
                )
```

```php
<?php  
echo CHtml::activeDropDownList($model,
                    'name',
                    array('1'=>'One','2'=>'Two'),
                    array('empty'=>'Select Option')
                );
//OR 
$type_list=CHtml::listData(
                    Types::model()->findAll(),
                    'id',
                    'type');
echo CHtml::activeDropDownList($model,
                    'name',
                    $type_list,
                    array('empty'=>'Select Option')
                ); 

?>
```
## DropDownList With Form
## Syntax:

```php
public static string dropDownList(CModel $model,
                    string $attribute, 
                    array $data, 
                    array $htmlOptions=array ( )
                )

<?php 
echo $form->dropDownList($model,
                    'name',
                    array('1'=>'One','2'=>'Two'),
                    array('empty'=>'Select Option')
                );
//OR 
$type_list=CHtml::listData(
                    Types::model()->findAll(),
                    'id',
                    'type'
                );
echo CHtml::activeDropDownList($model,
                    'name',
                    $type_list,
                    array('empty'=>'Select Option')
                );  
?>
```
## DropDownList With CHtml (Without Model Name)
## Syntax:

```php
public static string dropDownList(
                    string name, 
                    string select,
                    array data, 
                    array()
                )

<?php     
echo CHtml::dropDownList('Typelist[name]',
                    2,
                    array('1'=>'One','2'=>'Two'),
                    array('empty'=>'Select Option')
                );
//OR
$type_list=CHtml::listData(
                    Types::model()->findAll(),
                    'id',
                    'type'
                );
echo CHtml::dropDownList(
                    'Typelist[name]',
                    $selected_value='1',
                    $type_list,
                    array('empty'=>'Select Option')
                );
?> 
```

Output
```html
<select id="Typelist_name" name="Typelist[name]">
    <option value="">Select Option</option>
    <option value="1">One</option>
    <option value="2">Two</option>
</select>
```

## DropDownList Value In Controller

```php
<?php
.........
$model->attributes=$_POST['Typelist'];
echo $model->name;
.......
?>
```

## DropDownList Event
I wrote the code in registerscript for dropdownlist change event.

```php
<?php
Yii::app()->clientScript->registerScript('ddl-on-change','
    $("#Typelist_name").on("change",function()
    {
        var value = this.val();
        alert("DDL value"+value);
        //statement    
    });
');
?>
```

