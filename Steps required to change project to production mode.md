# Steps required to change project to production mode  
> Posted on October 8th, 2010 in PHP, Yii | No Comments ?
 
## Step 1: In config/main.php, add following element to "components" array

```php
'cache'=>array(
            'class'=>'system.caching.CApcCache',
        ),
```

### Step2: In **config/main.php**, add following element to " components['db'] " array

```php
'schemaCachingDuration' => 3600

```

### Step 3: Comment out the following line of index.php

```php
defined('YII_DEBUG') or define('YII_DEBUG',true);
```
### Step 4: Replace following line in index.php

```php
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
with

$yii=dirname(__FILE__).'/../yii/framework/yiilite.php';

```
> i.e. instead of including yii.php from framework, include **yiilite.php**
