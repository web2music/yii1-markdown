# Upload and resize image using YII  

> Posted on October 6th, 2010 in PHP, Yii | 8 Comments ?
Download and extract this file in protected/extensions directory.
 
In **config/main.php** file, add the following element to components array 

```php
'image'=>array(     
'class'=>'application.extensions.image.CImageComponent',            
'driver'=>'GD', 
) 
```
Now in action, insert the following code and modify paths

```php
$model->Picture = CUploadedFile::getInstance($model,'Picture');
if( is_object($model->Picture) )
{
    mkdir(getcwd().'/images/users/'.$model->Id."/");
    $name = getcwd().'/images/users/'.$model->Id."/".$model->Picture->getName();
    $model->Picture->saveAs($name);

    $image = Yii::app()->image->load($name);
    $image->resize(50, 50);
    $image->save();
}

```

Link Refer

https://www.yiiframework.com/extension/image