# Yii Url Manager Configuration

Yii framework is a great framework. We can maintain our websites with user friendly URL. In this article, I explained about "URL Manager" concept. Default yii url format is
www.sitename.com/index.php?r=controller_name/action_name&param1=value1&... We can rewrite this url to below format www.sitename.com/controller_name/action_name/param1/value1&... This is user friendly format URL. To hide "index.php" and to change url format use this article.

```php
Default Url Manager
Hide index.php
urlSuffix [.html,.php]
Change Url Route Path
```

### Default Url Manager
Normally "urlManager" was commented. To enable urlManager, we have to uncomment the urlManager. I showed the uncomment urlmanager was below File: config/main.php

```php
<?php
'urlManager'=>array(
     'urlFormat'=>'path',
     'rules'=>array(
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
        ),
	),
?>
```
Output Url

Now the url output will look like this
http://localhost/sample/index.php/site/index

### Hide index.php
We have to hide the "index.php/" in about url. To hide it, first we have to add or change the ".htaccess" file in below format. Some other formats are available on net.

> Changes In .htaccess

Add following source codes in .htaccess Note:Dont change .htaccess file inside protected Note:Create .htaccess file outside protectd folder File:

> protected/.htaccess

## RewriteEngine on

#### if a directory or a file exists, use it directly

```
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

# otherwise forward it to index.php
RewriteRule . index.php
```
### showScriptName

After changed .htaccess, We have to add and set "false" for "showScriptName" inside urlmanager. File: config/main.php

```php
<?php
'urlManager'=>array(
    'urlFormat'=>'path',
    'rules'=>array(
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
       ),
  ),
'showScriptName'=>false,
?>
```

Output Url

After the showScriptName changes, the url output will look like this
http://localhost/sample/site/index

### urlSuffix [.html,.php]
We changed our site url "http://localhost/sample/site/index" user friendly. But url dont have suffix like ".php", ".html", ".asp". Using "urlSuffix" we can add url suffix to url. File: config/main.php


```php
<?php
'urlManager'=>array(
    'urlFormat'=>'path',
    'rules'=>array(
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
       ),
     'showScriptName'=>false,
     'urlSuffix'=>'.html',
  ),
?>

```
Output Url

The final out of our url is http://localhost/sample/site/index.html

### Change Url Route Path
We can assign route variable name using 'routeVar' parameter in 'urlManager' component.The Defaults 'routeVar' value is 'r'.
```php
'urlManager' => array(
             ...
            'routeVar'=>'route',
             ...
  )
```  
now the site url will look like this
> http://127.0.0.1/project/index.php?route=site/index
