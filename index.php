<?php
    include("Parsedown.php");

    $Parsedown = new Parsedown();
    // $Parsedown->setSyntaxHighlight(true);
    $fileList = '';

    $fileList = '
    <thead>
    <tr>
        <th class="title">Title</th>
    </tr>
    </thead>';

    foreach (glob("./*.md") as $file) {
        //Include the file 
        // include($file);
        //$value = rand(0, 10000) * 2345678;
        // echo $file.'<br>';
        $file = str_replace(
            array('./','.md',' '),
            array('','','%20'),
            $file);
        $fileList .= "<tr>
                            <td><button type=\"button\" class=\"btn btn-success block \" onClick=(btn('$file')) \">".str_replace('%20',' ',$file)."</button></td>
                      </tr>";
    }
    // $fileList .= '</table>';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="md-github.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
    .col-sm-3 {
        height: 750px;
        overflow-y: scroll;
        padding: 0px 0px;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }


    .block {
        display: block;
        width: 100%;
        border: none;
        background-color: #4CAF50;
        /* padding: 14px 28px; */
        font-size: 14px;
        cursor: pointer;
        text-align: left;
    }
    </style>
    <title>MarkDown</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row">

        </div>
        <div class="row">
            <div class="col-sm-9">
                <div class="table-responsive">
                    <div style="overflow-x:auto;">

                        <span id="content">content</span>

                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <!-- <div id="dsTable" class="container"> -->
                <table class="table table-hover table-bordered table-responsive-menu">
                    <tbody>
                        <?php echo $fileList; ?>
                    </tbody>
                </table>
                <!-- </div> -->
            </div>
        </div>

    </div>


    <script type="text/javascript">
    if ($("#content").html() == 'content') {
        // console.log('empty');
        btn('readme');

    }

    function btn(msg) {
        // alert(msg);
        $.post("apiContent.php", {
                f: msg,
            },
            function(data, status) {
                // alert("Data: " + data + "\nStatus: " + status);
                document.getElementById("content").innerHTML = data;
            });
    }
    </script>

</body>

</html>