## CHtml radioButtonList Ajax Submit
I added the code to submit the yii form When we select the value from Chtml radio button list box.

```html
<div class="search-form" style="display:block">

    <div class="wide form">
    <?php 
        $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); 
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'type'); ?>
        <?php 
            $types=array("1"=>"Type1","2"=>"Type2");
            echo $form->radioButtonList(
                    $model,
                    'type',
                    $types,
                    array(
                        'template'=>'{input}{label}',
                        'separator'=>'',
                        'labelOptions'=>array('style'=>'padding-left:13px'),
                        'style'=>'float:left;',
                        'onclick'=>CHtml::ajax(array(
                            'success'=>"$('.search-form form').submit()",
                        ))
                    )                              
                );
        ?>
    </div>
    </div>
</div>
```