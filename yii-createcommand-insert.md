# Yii createCommand Insert

Functions
`
function insert($table, $columns)
The insert() method builds and executes an INSERT SQL statement.
`

## Sample 1
 
```
<?php
$user=Yii::app()->db->createCommand()
        ->insert(
            'tbl_user',
            array(
                'username'=>'bsourcecode',
                'usertype'=>1,
                'password'=>'hai'
            )
        );
?>
```

## Output


```sql
INSERT INTO `tbl_user` 
    (`username`, `usertype`,'password') 
    VALUES 
    (:username,:usertype, :password)
```

## Sample 2
 
```php
<?php
$name='india';
$isactive=1;
$command= Yii::app()->db->createCommand(
        "INSERT INTO regionmaster 
        (`regionname`,`isactive`)
        VALUES 
        (:name,:isactive)");
$command->bindValue(':name', $name);
$command->bindValue(':isactive', $isactive);
$sql_result = $command->execute();
?>

```

Output

```sql
INSERT INTO regionmaster 
    (`regionname`,`isactive`)
    VALUES 
    (:name,:isactive)`

```