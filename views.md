# Yii Views

## Display select widget for form

Write at your view file:

```php
<?php
$products = CHtml::listData(Product::model()->findAll(), 'id', 'title');
echo $form->dropDownListRow($model, 'product_id', $products, [
    'empty' => 'Select product',
]);
?>
```

## Add CSS file, add javascript file

You can publish js, css files from your folder to `assets` directory:

```php
$assetsPath = Yii::app()->getAssetManager()->publish(__DIR__ . '/assets/', false, -1, YII_DEBUG);
Yii::app()->clientScript->registerCssFile($assetsPath . '/css/anti-mite-widget.css');
Yii::app()->clientScript->registerScriptFile($assetsPath . '/js/scripts.js');
```