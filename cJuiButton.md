# CJui Button

>Using button type we will show different type of button in cjuibutton of yiiframework. Possible types are submit, button, link, radio, checkbox, buttonset.

# CJuiButton: Button
Submit Button

## User Name :

```php 
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
    'name'=>'submit',
    'caption'=>'Submit',
    'htmlOptions'=>array(
        'style'=>'background:#0064cd;color:#ffffff;',
    ),
));
?>
```

# Default Button
```php
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
    'name'=>'button',
    'caption'=>'Instructions',
    'onclick'=>new CJavaScriptExpression('function(){alert("Enter User Name"); this.blur(); return false;}'),
));
?>
```

# CJuiButton: Link
Basic Link Link To Bsourcecode.com
```php
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
    'name'=>'link',
    'caption'=>'Link To Bsourcecode.com',
    'buttonType'=>'link',
    'url'=>'http://www.bsourcecode.com',
));
?>
```

# CJuiButton: Link->htmlOptions
Link To Bsourcecode.com

```php
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
    'name'=>'cjui-link',
    'caption'=>'Link To Bsourcecode.com',
    'buttonType'=>'link',
    'url'=>'http://www.bsourcecode.com',
    'htmlOptions'=>array(
        'style'=>'color:#ffffff;background: #0064cd;'
    ),
    //'onclick'=>new CJavaScriptExpression('function(){alert("Enter User Name"); this.blur(); return false;}'),
    
));
?>
```

# CJuiButton: radio
Single Radio Button
Single Radio

```php
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
    'name'=>'ratio-button',
    'buttonType'=>'radio',
    'caption'=>'Single Radio'
));
?>
```

# Radio Button Set
Choice 1
Choice 2
Choice 3
```php
<?php $radio = $this->beginWidget('zii.widgets.jui.CJuiButton', array(
    'name'=>'radio-btn',
    'buttonType'=>'buttonset',
)); ?>
```
```html
<input type="radio" id="radio1" name="radio" value="1" /><label for="radio1">Choice 1</label>
<input type="radio" id="radio2" name="radio" value="2" checked="checked"/><label for="radio2">Choice 2</label>
<input type="radio" id="radio3" name="radio" value="3" /><label for="radio3">Choice 3</label>

<?php $this->endWidget();?>
```

# CJuiButton: Checkbox
Single Checkbox Button
Single Checkbox
```php
<?php
$this->widget('zii.widgets.jui.CJuiButton',array(
    'name'=>'checkbox-button',
    'buttonType'=>'checkbox',
    'caption'=>'Single Checkbox'
));
?>
```

# Checkbox Button Set
Checkbox 1
Checkbox 2
Checkbox 3
```php
<?php $radio = $this->beginWidget('zii.widgets.jui.CJuiButton', array(
    'name'=>'checkbox-btn',
    'buttonType'=>'buttonset',
    'htmlTag'=>'span',
)); ?>
<input type="checkbox" id="check1" value="1" /><label for="check1">Checkbox 1</label>
<input type="checkbox" id="check2" value="2" /><label for="check2">Checkbox 2</label>
<input type="checkbox" id="check3" value="3" /><label for="check3">Checkbox 3</label>
<?php $this->endWidget();?>
```
