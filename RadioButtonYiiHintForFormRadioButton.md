## Yii Hint For Form Radio Button

>This tutorial will help you to style the radio button list on form. Using template, separator, style and label options, We can give proper style to radio button.

```html
<div class="row">
<?php echo $form->labelEx($model,'isactive'); ?>
<?php
    echo $form->radioButtonList(
            $model,
            'isactive',
            array('1'=>'Active','0'=>'Inactive'),
            array(
                'template'=>'{input}{label}',
                'separator'=>'',
                'labelOptions'=>array(
                    'style'=>'padding-left:13px;width: 60px;float: left;'
                ),
                'style'=>'float:left;',
            )                              
        );
?>
<div style="clear: both;"></div>
<?php echo $form->error($model,'isactive'); ?>
</div>
```
