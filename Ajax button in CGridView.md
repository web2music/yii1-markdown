# Ajax button in CGridView
Posted on October 4th, 2010 in jQuery, PHP, Yii | No Comments ?
To add an ajax button to CGridView modify the buttons column  as

```php

'columns' => array(
    'id',
    'name',
    array(
        'class'=>'CButtonColumn',
        'template' => '{view} {update} {delete}',
        'buttons'=>array(
            'view' => array(
                'url'=>'"index.php?r=admin/view&id=".$data->user_id."&m=users"',
                     'click' => "function (){ $('#viewTab').load($(this).attr('href'));return false; }"
            )
        ),
    ),
)

```