# Using external PHP libraries with the Yii framework

Yii is an easy to use and flexible PHP5 framework. It comes with all essential functionality needed to quickly create any sort of web application. Because Yii is not intended to be an all-in-one solution, you can easily extend it to fit your needs. On the Yii web site there are several extensions available.

But you are not restricted to the use of Yii extensions. If there is an existing library out there you like, use it.

Create a vendors folder in your application folder:
protected/vendors
Copy your libraries to the newly created vendors folder.
Add the folder to your include path in your configuration file **(protected/config/main.php):**

```php
...
'import'=>array(
  'application.models.*',
  'application.components.*',
  'application.vendors.*',
),
...
```
>Yii uses the autoload functionality of PHP to automatically load scripts. For this to work in Yii, the file name must be equal to the class name. So for example, if you want to use Simplepie the script file must be named protected/vendors/Simplepie.php.

>You can then simply create an object in any controller with:

```php
$feed = new Simplepie;
```

>Simplepie uses the idna_convert class for international domain names. This class must also reside in the protected/vendors folder

>But that is not all. You can also use library collections, like Zend Framework or eZ Components, that utilise PHP's autoload. And you can use them at the same time using spl_autoload_register().

>The easiest place to register the libraries is in the entry script. This way they can be loaded from any controller.

>You first have to unregister Yii autoload, because Yii raises an error if a class isn't found.
Next step is to include the autoload scripts and register the autoload functions of the libraries.
The last step is to register Yii autoload again, so everything works as before.
The final entry script (index.php) will look like this:

```php
<?php
$yii='/path/to/yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

require($yii);

// unregister Yii autoload
spl_autoload_unregister(array('YiiBase','autoload'));

// register Zend Framework
Yii::import('application.vendors.ZendFramework.library.*');
require('Zend/Loader/Autoloader.php');
spl_autoload_register(array('Zend_Loader_Autoloader', 'autoload'));


// register eZ Components
Yii::import('application.vendors.ezcomponents.*');
require('Base/src/base.php');
spl_autoload_register(array('ezcBase', 'autoload'));

// re-register Yii autoload
spl_autoload_register(array('YiiBase', 'autoload'));
Yii::createWebApplication($config)->run();
```

Now you can create any object without worrying where the class scripts are located.