# Yii createCommand Delete
## Functions

```php
function delete($table, $conditions='', $params=array())

```
>The delete() method builds and executes a DELETE SQL statement.`'$table'` is a table name of delete from.`'$conditions'` and `'$params'` is like where condition of delete statement

Sample

```php
<?php
$user=Yii::app()->db->createCommand()
        ->delete('tbl_user', 'user_id=:id', array(':id'=>2));
?> 

```

Output

```sql
DELETE FROM `tbl_user` WHERE user_id=:id

```