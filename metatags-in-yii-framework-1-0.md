# MetaTags In Yii Framework 1.0

In yii framework 1.0, Registers a meta tag that will be inserted in the head section (right before the title element) of the display page. Each call of this method will cause a rendering of new meta tag like keywords, description, robots etc, even if their attributes are equal.

-   Syntax
-   Keywords
- Content-type
- Description
- Cookie
- ROBOTS
- Cache-control
- Expires
- Pragma
- Author
- refresh
- Custom Meta

### Syntax

public CClientScript registerMetaTag(string $content, string $name=NULL, string $httpEquiv=NULL, array $options=array ( ), string $id=NULL)
$content	string	content attribute of the meta tag
$name	string	name attribute of the meta tag.
If null, the attribute will not be generated
$httpEquiv	string	http-equiv attribute of the meta tag. If null, the attribute will not be generated
$options	array	other options in name-value pairs (e.g. 'scheme', 'lang')
$id	string	Optional id of the meta tag to avoid duplicates
{return}	CClientScript	the CClientScript object itself (to support method chaining, available since version 1.1.5).


### Keywords

Yii::app()->clientScript->registerMetaTag('Keyword1, keyword2', 'keywords');
//Output
<meta name="keywords" content="Keyword1, keyword2" />
Description

Yii::app()->clientScript->registerMetaTag('Site Description Details', 'description');
//Output
<meta name="description" content="Site Description Details" />
Content-type

Yii::app()->clientScript->registerMetaTag('text/html;charset=UTF-8', null, 'content-type');
//Output
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
Cookie

Yii::app()->clientScript->registerMetaTag('userid=xyz; expires=Wednesday, 08-Aug-15 23:59:59 GMT;', null, 'cookie');
//Output
<meta http-equiv="cookie" content="userid=xyz; expires=Wednesday, 08-Aug-15 23:59:59 GMT;" />
ROBOTS

Yii::app()->clientScript->registerMetaTag('NOINDEX, NOFOLLOW', 'ROBOTS');
//Output
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />

Yii::app()->clientScript->registerMetaTag('INDEX, NOFOLLOW', 'ROBOTS');
//Output
<meta name="ROBOTS" content="INDEX, NOFOLLOW" />		

Yii::app()->clientScript->registerMetaTag('NOINDEX, FOLLOW', 'ROBOTS');
//Output
<meta name="ROBOTS" content="INDEX, NOFOLLOW" />		
Cache-control

Yii::app()->clientScript->registerMetaTag('max-age=0', null, 'cache-control');
//Output
<meta http-equiv="cache-control" content="max-age=0" />

Yii::app()->clientScript->registerMetaTag('no-cache', null, 'cache-control');
//Output
<meta http-equiv="cache-control" content="no-cache" />
Expires

Yii::app()->clientScript->registerMetaTag('0', null, 'expires');
//Output
<meta http-equiv="expires" content="0" />

Yii::app()->clientScript->registerMetaTag('Mon, 16 June 2014 23:59:59 GMT', null, 'expires');
//Output
<meta http-equiv="expires" content="Mon, 16 June 2014 23:59:59 GMT" />
Pragma

Yii::app()->clientScript->registerMetaTag('no-cache', null, 'pragma');
//Output
<meta http-equiv="pragma" content="no-cache" />
Author

Yii::app()->clientScript->registerMetaTag('Author Name', 'author');
//Output
<meta name="author" content="Author Name" />
refresh

Yii::app()->clientScript->registerMetaTag('30', null, 'refresh');
//Output
<meta http-equiv="refresh" content="30" />

Yii::app()->clientScript->registerMetaTag('30', null, 'refresh',array('url'=>'http://www.bsourcecode.com'));
//Output
<meta http-equiv="refresh" content="30" url="http://www.bsourcecode.com"  />
Custom MetaTag

Yii::app()->clientScript->registerMetaTag('637502, 637503', 'zipcode');
//Output
<meta name="zipcode" content="637502, 637503" />
Yii 1.0 name-value pairs of meta tag.

Yii::app()->clientScript->registerMetaTag(null, null, null,array('charset'=>'utf-8'));
//Output
<meta charset="utf-8"/>
