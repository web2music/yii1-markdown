## Yii 1.0 Ajax search using Radio Buttons
>Would you like to trigger one ajax request While clicking on Radio button, This post will help you to get radio button ajax request.

>In _search.php (form)
```html
<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>
<div class="row">
    <?php echo $form->radioButtonList(
                $model,
                'status',
                array(
                    '1'=>'Active',
                    '2'=>'Inacitve'
                ),
                array(
                    'template'=>'{input}{label}',
                    'separator'=>'',
                    'labelOptions'=>array('style'=>'padding-left:13px'),
                    'style'=>'float:left;',
                    'onclick'=>CHtml::ajax(array(
                        'success'=>"$('.search-form form').submit()",
                    ))
                )                              
        );
    ?>
</div> 
<?php $this->endWidget(); ?>
</div>
```
>In Model.php

```php
public function searchfiles()
{
    // Warning: Please modify the following code to remove attributes that
    // should not be searched.
    $criteria=new CDbCriteria;
    if($this->status==1)
    {
        $criteria->condition=""            
    }else if($this->status==2)
    {
        $criteria->condition="" ;      
    }                
    return new CActiveDataProvider(
                $this, array('criteria'=>$criteria)
            );
}
```