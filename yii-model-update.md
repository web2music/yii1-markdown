# Yii Model Update

>This tutorial will help you to get the information about yii update function. I added the source code to update one or multiple records using different update function in yii framework model.


```php
<?php
$model=User::model()->updateByPk(
            $ID,
            array("status"=>'DECLINED')
            );

$model=User::model()->updateAll(array(
            'first_name'=>"$firstname",
            'email'=>"$email"
            ),
            "UserID=$id and status IS NULL "
        );

$model=User::model()->updateAll(
            array('status' => 1), 
            'UserID ='.$userid
            );
?> 
```