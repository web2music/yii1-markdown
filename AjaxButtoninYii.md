## CHtml Ajax Button in Yii

Example 1:
In view.php page Write Follwing source code
```php
<?php
echo CHtml::ajaxButton(
        'Submit request',
        array('test/function'),
        array('data'=>array('test'=>$model->id)),
        array('update'=>'#update_selector')
        //this is the update selector of yours $('#update_selector').load(url);
    );
?>
```
>In controller.php write following source code
```php
public function actionFunction() 
{
    if(Yii::app()->request->isAjaxRequest)
    {
        $this->renderPartial("_form",array('model'=>$model),false,true);
        Yii::app()->end();
    }
}
```
Example 2:
```php
<?php
echo CHtml::ajaxButton(
            'Save',
            array('user/useraction'),
            array('data'=>array(
                    'id'=>$model->year_id,
                    'usertype'=>'js:$("#usertype").val()',
                    'username'=>'js:$("#username").val()',
                )
                'type'=>'POST', 
            );
?>
```