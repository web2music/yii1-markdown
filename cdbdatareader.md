# CDbDataReader
CDbDataReader reads the rows from a result.

>CDbDataReader Query

```php
<?php
$cdbdatareader = Yii::app()->db->createCommand()
        ->select('username, password')
        ->from('usermaster')
        ->query();    
?>
```

>columnCount & rowCount

```php
<?php
        echo $cdbdatareader->rowCount;
        echo $cdbdatareader->columnCount;
?>

```
>CDbDataReader Using read(), next()

```php
<?php
    $record=$cdbdatareader->read();
    echo $record['username'];
    $cdbdatareader->next();
    $record=$cdbdatareader->read();
    echo $record['username'];
?>

```

>CDbDataReader Using foreach() & while()

## foreach()

>We can also retrieve the rows of data in CDbDataReader by using foreach. We can go forward only and cant take it backward.

```php
foreach($cdbdatareader as $records)
{
    echo $records->username;    
}

```
>while()

```php
<?php
while (($records = $cdbdatareader->read()) !== false)
{
    echo $records['username'];
}
?>

```