# HTML TO PDF IN YII

## SOURCECODE

>html2pdf.php

```php
<?php
    ob_start();
    echo $this->renderPartial('htmlpage'); 
    $content = ob_get_clean();

    Yii::import('application.extensions.tcpdf.HTML2PDF');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
    //  $html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content,false);
        $html2pdf->Output("pdfdemo.pdf");
        
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>
```

>htmlpage.php


```css
<style type="text/css">
h1{
    color:red;
    text-align: center;
}
table{
    margin: auto;
    border: 1px solid gray;
}
table td,table th{
    border: 1px solid gray;
    border-collapse: collapse;
    padding: 5px;
}
</style>
```

```html
    <div>
        <img src="http://static.yiiframework.com/files/logo/yii.png" title="yii logo" />
        <h1> YII HTML TO PDF DEMO </h1>
    </div>
    <br /><br /><br />
    <table>
        <tr><th>TITLE</th><th>LINK</th></tr>
        <tr><td>BSOURCECODE</td><td><a href="http://www.bsourcecode.com/">http://www.bsourcecode.com</a></td></tr>
        <tr><td>DEMO BSOURCECODE</td><td><a href="http://demo.bsourcecode.com/">http://demo.bsourcecode.com</a></td></tr>
        <tr><td>YII FRAMEWORK</td><td><a href="http://www.yiiframework.com">http://www.yiiframework.com</a></td></tr>
    </table>
```
>HtmltopdfController.php

```php
<?php
class HtmltopdfController extends Controller
{    
	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionPdf()
	{
		$this->renderPartial('html2pdf');
	}
}
?>
```