# CJui AutoComplete
## CJuiAutoComplete


>Ajax CJuiAutoComplete
## /** View.php ------------**/

<h1>CJuiAutoComplete</h1>

```php
<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'normal',
    'source'=>array('ac1','ac2','ac3'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
    ),
));
?>
```
```html
<br />
<br />
<hr />
<h1>Ajax CJuiAutoComplete</h1>
```
```php
<?php
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'ajaxrequest',
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'1',
    ),
    'source'=>$this->createUrl("cjuiautocomplete/ajax"),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
    ),
));
?>
```

>Write the following code in cjuiautocomplete/ajax action
```php
    public function actionAjax(){
        $request=trim($_GET['term']);
        if($request!=''){
            $model=Yiidemo::model()->findAll(array("condition"=>"name like '$request%'"));
            $data=array();
            foreach($model as $get){
                $data[]=$get->name;
            }
            $this->layout='empty';
            echo json_encode($data);
        }
    }
```    