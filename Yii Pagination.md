## Yii Pagination :CLinkPager
Create New Look For Pager Of Great Yii.
Yii pagination widget is the one of the feature in yii framework. Listview and gridview have the pagination and it have the some basic properties, design and values for that properties. We can change the pager propertices and make new look for pagination.

```html
ClinkPager Properties
Pagination Lable With Text
Pagination Lable With Different Css
Pagination Lable With Image
Pagination HTML Options
ClinkPager Properties
Header Text
Previous Page Text
Next Page Text
First Page Text
Last Page Text
Footer Text
Display Number Of Button In Pagination
First Page Css Class
Last Page Css Class
Previous Page Css Class
Next Page Css Class
Internal Page Css Class
Selected Page Css Class
Hidden Page Css Class
Add Css file For Pagination
HTML Options
```

Pagination Lable With Text
----------------------
In this topic i added the sourcecode for header text,previous page text,next page text,first page text, last page text and footer page text of pagination. I explained this topic for listview and gridview. After this i will give sourcecode for listview.
For Listview Pagination


```php
<?php
$this->widget('zii.widgets.CListView', array(
'dataProvider'=>$dataProvider,
'itemView'=>'_userlist',
    'summaryText'=>'',//'Result {start} - {end} of {count} results'
    'pager' => array(
        'cssFile'=>Yii::app()->theme->baseUrl."/css/pagination.css",
        'header' => 'Go To Page',
        'prevPageLabel' => 'Previous',
        'nextPageLabel' => 'Next',
        'firstPageLabel'=>'First',
        'lastPageLabel'=>'Last',
        'footer'=>'End',//defalut empty
        'maxButtonCount'=>4 // defalut 10                    
    ),
)); ?>
```

For Gridview Pagination

```php
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'user-grid',
    'dataProvider'=>$model->searchactive(),
    'columns'=>array(..),
    'pager' => array(
        'cssFile'=>Yii::app()->theme->baseUrl."/css/pagination.css",
        'maxButtonCount'=>4,
        'header' => 'Go To Page',
        'prevPageLabel' => 'Previous',
        'nextPageLabel' => 'Next',
        'firstPageLabel'=>'First',
        'lastPageLabel'=>'Last'
        'footer'=>'End'
    ),
));            
?>
```

Pagination Lable With Different Css
--------------------------
Top of the page, i added the properties of ClinkPager. We can assign the different css class for first page, last page, previours page, next page, internal page (ex li), selected page or current page and hidden page

```php
<?php 
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'template'=>'{pager}{items}',
    'pager'=>array(
        'header'=>'',
        'prevPageLabel'=>'pre',
        'nextPageLabel'=>'',
        'firstPageLabel'=>'First',
        'lastPageLabel'=>'Last',
        // css class         
        'firstPageCssClass'=>'pager_first',//default "first"
        'lastPageCssClass'=>'pager_last',//default "last"
        'previousPageCssClass'=>'pager_previous',//default "previours"
        'nextPageCssClass'=>'pager_next',//default "next"
        'internalPageCssClass'=>'pager_li',//default "page"
        'selectedPageCssClass'=>'pager_selected_li',//default "selected"
        'hiddenPageCssClass'=>'pager_hidden_li'//default "hidden"                                    
    ),
)); 
?>
```

Css Class For Pagination

```css
<style type="text/css">
ul.yiiPager .pager_first a{
    color:red;
}
ul.yiiPager .pager_last a{
    color:green;
}
ul.yiiPager .pager_next a{
    color:blue;
}
ul.yiiPager .pager_previous a{
    color:yellow;
}
</style>
```

Pagination Lable With Image
-------------

The default value for pagination labels are text. We can change it from "Text" label to "Image" label. We will apply image to pagination in two methods. First one, Assign a image "" tag to pagination label directly (inline). Second one, Assign a image using "CSS" class.

Inline Image
-----------

```php
<?php 
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'template'=>'{pager}{items}',
    'pager'=>array(
        'header'=>'',
        'cssFile'=>Yii::app()->theme->baseUrl."/css/pagination-widget.css",
        'prevPageLabel'=>'<img src="'.Yii::app()->theme->baseUrl.'/images/left_arrow.gif" />',
        'nextPageLabel'=>'<img src="'.Yii::app()->theme->baseUrl.'/images/right_arrow.gif" />',
        'firstPageLabel'=>'First',
        'lastPageLabel'=>'Last'
    ),
)); 
?>
```

Assign Image Using CSS
-------------
Pagination Lable With Image Using Css. The default pagination was created using "ul" tag. The "`<div class='pager'>`" is the parent tag of ul. Using this tag i added image to pager.

```php
<?php 
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'template'=>'{pager}{items}',
    'pager'=>array(
        'header'=>'',
        'cssFile'=>Yii::app()->theme->baseUrl."/css/pagination-widget.css",
        'prevPageLabel'=>'',
        'nextPageLabel'=>''
    ),
)); ?>    

<style type="text/css">        
.pager ul li.previous a{
    height: 50px;
    width: 25px;
    border-style:none;
    display:block;
    background:url(../images/left_arrow.gif) no-repeat;
}
.pager ul li.next a{
    height: 50px;
    width: 25px;
    border-style:none;
    display:block;
    background:url(../images/right_arrow.gif) no-repeat;
}
</style>
```

Pagination HTML Options
-----------------------
Every yii members have the knowledge of "htmlOptions". It is also available here. Using "htmlOptions" We can assign the html properties for pager like "id","class","style" etc.

```php
<?php 
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'template'=>'{pager}{items}',
    'pager'=>array(
        'header'=>'',
        'prevPageLabel'=>'',
        'nextPageLabel'=>'',

        'htmlOptions'=>array(
            'class'=>'mypager_class'
            'style'=>'',
            'id'=>'mypager_id'
        ),      
    ),
)); ?>
```  
