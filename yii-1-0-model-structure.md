# Yii 1.0 Model Structure
>By default model classes are available within the protected/models dirctory. Every class must be inherit from the CModel or subclass. Yii difines two sub classes. They are CActiveRecord, CFormModel.

>CActiveRecord is conntected with database table model (classes) and it represent the table structure.

>CFormModel is the basic model class and conntected to HTML forms.

1. Class Extend CFormModel Structure
2. Class Extend CActiveRecord Structure
3. Available Validators By Default
4. Model Events

## Class Extend CFormModel Structure

```php
<?php
class ClassName extends CFormModel {
    // Attributes...
    public $Attribute_information;
    // Methods...
    public function rules() {}
    public function attributeLabels() {}
}
?>

```

## Class Extend CActiveRecord Structure

```php
class ClassName extends CActiveRecord {
    // Methods...
    public static function model($className=__CLASS__) {}
    public function tableName() {}
    public function rules() {}
    public function relations() {}
    public function attributeLabels() {}
    public function search() {}
}

```

Available Validators By Default
* boolean
* captcha
* compare
* date
* default
* email
* exist
* file
* filter
* in
* length
* match
* numerical
* required
* safe
* type
* unique
* unsafe
* url

## More Validation Futures In Yii Model
* Creating Your Own Validator
* Applying Filters like strip_tags, trim etc
* Scenarios Based Validation

## Model Events
* afterConstruct()
* afterDelete()
* afterFind()
* afterSave()
* afterValidate()
* beforeDelete()
* beforeFind()
* beforeSave()
* beforeValidate()