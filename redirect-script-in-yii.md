# Redirect Script in Yii

When we try to load one page, we can redirect that request to another page. Yii framework support redirection method. Yii redirect function is used to redirect the one controller action to another controller action and also we can redirect within controller. The types of redirections are

#### Redirect Anywhere
```php
Redirect Within Controller
Redirect Between Controller
```

#### Redirect Anywhere
```php

// We can redirect anywhere in application
Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("user/view"));
// We can redirect anywhere in application with parameter
Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("user/view",array("id"=>$id)));
```

#### Redirect Within Controller

```php
//Redirect within controller
$this->redirect(array("admin"));
$this->redirect(array("admin",array("id"=>$id));
```

### Redirect Between Controller
```php
//Redirect between controller
$this->redirect(array("site/login"));
$this->redirect(array("error/display",array("error"=>$error));
```
