# Yii Maintenance Mode
This tutorial will useful to change your yiiframework website to maintenance mode. User can access maintenance file only.


+ Maintenance Controller
+ Maintence File
+ Maintenance Controller
+ Make or Point the controller action to maintenance mode.

```php
return array(
    'catchAllRequest'=> array('site/maintenance'),
    ............    
```

## Maintence File
Make or Point the file to maintenance mode. If the condition [file_exists()](https://www.php.net/manual/en/function.file-exists.php) return false, The maintenance mode will be off or not work.

```php
'catchAllRequest'=>file_exists(dirname(__FILE__).'/.maintenance')? array('site/maintenance') : null,
```
