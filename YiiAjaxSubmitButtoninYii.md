## Ajax Submit Button in Yii
>Normally we will use the form submit action via post or get method with page submit option. With out page refresh we can submit the form using ajax concept. It will be faster than page reload. This post will help you to under stand AjaxSubmitButton instead of submitButton in yii.

ajaxSubmitButton With Form
```html
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        'enableAjaxValidation'=>false,
    )); 
?>

<div class="row">
    <?php echo $form->labelEx($model,'companyname'); ?>
    <?php echo $form->textField($model,'companyname',
        array('size'=>60,'maxlength'=>1024)); ?>
    <?php echo $form->error($model,'companyname'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'eventname'); ?>
    <?php echo $form->textField($model,'eventname',
        array('size'=>60,'maxlength'=>1024)); ?>
    <?php echo $form->error($model,'eventname'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'eventlist'); ?>
    <?php echo $form->dropDownList($model,'eventlist',
        $eventlist,array('empty'=>'Select Event')); ?>
    <?php echo $form->error($model,'eventlist'); ?>
</div>

<div class="row buttons">
```
```php
<?php 
    //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
    echo CHtml::ajaxSubmitButton('Save', 
        CHtml::normalizeUrl(array('eventmaster/category')), 
        array(
            'data'=>'js:jQuery(this).parents("form").serialize()+
                "&request=added"',       
            'success'=>'function(data){
                $("#eventlist").html(data);
            }'
        ), 
        array(
            'id'=>'ajaxSubmitBtn', 
            'name'=>'ajaxSubmitBtn'
        )); 
?>
```
```html
</div>
```

## Yii CHtml ajaxSubmitButton Description
>I added ajaxsubmitbutton instead of submitButton. When I click the submit button I will send form data through ajax post and I will do controller work for insert and I render the listdata using CHtml::listData() and I will set the response value to eventlist dropdownlist.

So I am updating the event list without page refresh using ajaxsubmitbutton of yii.