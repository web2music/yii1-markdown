# Yii CHttpRequest
>CHttpRequest is equal to '$_SERVER'. we are using '$_SERVER' variable to get the details of request, scripts, headers etc. We can get this information using CHttpRequest class. We can access the CHttpRequest via Yii::app()->request. It will return browser accept types, baseurl, csrftoken, csrftoken name, host name, request type [POST, AJAX, GET, PUT, DELETE], request Uri, url, url Referrer, user Agent, user Host 
Address and more

* Yii::app()->request->acceptTypes
* Yii::app()->request->baseUrl
* Yii::app()->request->csrfToken
* Yii::app()->request->csrfTokenName
* Yii::app()->request->hostInfo
* Yii::app()->request->isAjaxRequest
* Yii::app()->request->isDeleteRequest
* Yii::app()->request->isFlashRequest
* Yii::app()->request->isPostRequest
* Yii::app()->request->isPutRequest
* Yii::app()->request->requestUri
* Yii::app()->request->url
* Yii::app()->request->urlReferrer
* Yii::app()->request->userAgent
* Yii::app()->request->userHostAddress

## Redirect Script
[gad]

>Note: sitepath is "http://127.0.0.1/httprequest"
Yii::app()->request->acceptTypes
accetTypes is used to get the browser accept types. Return Type: String

## Example

```php
echo Yii::app()->request->acceptTypes
```
Output

```php
text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Yii::app()->request->baseUrl
baseUrl will return the relative url of application Return Type: String
```
## Example
```php
<?php
echo Yii::app()->request->baseUrl;
Yii::app()->request->setBaseUrl('yiiframework');
echo "<br/>";
echo Yii::app()->request->getBaseUrl();
?>
```

Output
```php
/httprequest
yiiframework
Yii::app()->request->csrfToken
```

This function return the random token to validate Cross-Site Request Forgery (CSRF) Return Type: String

## Example

```php
echo Yii::app()->request->csrfToken;
```

Output
```php
e7376eb9d043f4c819a7f31b16397840b1d5adce
Yii::app()->request->csrfTokenName
```

Name of the csrfToken to prevent CSRF Return Type: String

Example

echo Yii::app()->request->csrfTokenName;
Output
```html
YII_CSRF_TOKEN
Yii::app()->request->hostInfo
```
It will return the host part of yii application Return Type: String

Example

```php
echo Yii::app()->request->hostInfo;
```

Output
```php
http://127.0.0.1
Yii::app()->request->isAjaxRequest
```

It will return true If the request is happened using ajax Request. Using this we can restict the request for actions. Return Type: boolean

Example

```php
class SiteController extends Controller{
    public function actionDelete($id){
        if(Yii::app()->request->isAjaxRequest){
            // request via ajax    
        }else{
            // it is not ajax request 
        }            
    }
}
Yii::app()->request->isDeleteRequest
```
It is used to find the 'DELETE' request. Return Type: boolean

Example

```php
class SiteController extends Controller{
    public function actionDelete($id){
        if(Yii::app()->request->isDeleteRequest){
            // DELETE Request
        }else{
            // it is not DELETE request 
        }            
    }
}
Yii::app()->request->isFlashRequest
```
To find flash and flex request, we can use this. Return Type: boolean

Example

```php
class SiteController extends Controller{
    public function actionDelete($id){
        if(Yii::app()->request->isFlashRequest){
            // DELETE Request
        }else{
            // it is not DELETE request 
        }            
    }
}
Yii::app()->request->isPostRequest
```
To find the POST request, we can use this method. Return Type: boolean

Example

```php
class SiteController extends Controller{
    public function actionUpdate($id){
        if(Yii::app()->request->isPostRequest	){
            // DELETE Request
        }else{
            // it is not DELETE request 
        }            
    }
}
Yii::app()->request->isPutRequest
```
To find the PUT request, we can use this method. Return Type: boolean

Example

```php
class SiteController extends Controller{
    public function actionUpdate($id){
        if(Yii::app()->request->isPutRequest	){
            // DELETE Request
        }else{
            // it is not DELETE request 
        }            
    }
}
Yii::app()->request->requestUri
```
Currently requested url or current page url Return Type: String

Example
```php
echo Yii::app()->request->requestUri;
```
Output
```php
/httprequest/index.php?r=site/index
Yii::app()->request->url
```

Currently requested url or current page url Return Type: String

Example

```php
echo Yii::app()->request->url;
```

Output
```php
/httprequest/index.php?r=site/index
Yii::app()->request->urlReferrer
```
>Return the referrer url. Using this we can get previous page url. It will return if not set Return Type: String

Example

```php
echo Yii::app()->request->urlReferrer;
```

Output
```php
http://127.0.0.1/httprequest/index.php?r=site/login
Yii::app()->request->userAgent
```

Return the referrer url. Using this we can get previous page url. It will return if not set Return Type: String

Example
```php
echo Yii::app()->request->userAgent;
```
Output
```
Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36
Yii::app()->request->userHostAddress
```

return the user ip address Return Type: String

Example
```php
echo Yii::app()->request->userHostAddress;
```

Output
```
127.0.0.1
Redirect Script
Redirect Script Tutorial
```