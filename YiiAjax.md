## Yii Ajax
* Ajax Submit Button
* Ajax Button
* Ajax Link
* Ajax Support 
* Ajax Submit Button
* Ajax submit button can submit the current form in POST method.

```php
public static string ajaxSubmitButton(
    string $label, 
    mixed $url, 
    array $ajaxOptions=array ( ), 
    array $htmlOptions=array ( )
)
```
```
$lable - the button label
$url - the URL for the AJAX request. If empty, it is assumed to be the current URL.
$ajaxOptions - data, success, update, replace,etc..;
$htmlOptions - add HTML attributes here ex. name, id etc
```
## Sample code for ajaxSubmitButton:
```php
<div class="form">
<?php 
$form=$this->beginWidget('CActiveForm,array(
    'id'=>'user-form',
    'enableAjaxValidation'=>false,
));
?>
```
```html
<div class='row'>
    <?php echo $form->labelEx($model,'eventname'); ?>
    <?php echo $form->textField($model,'eventname'); ?>
    <?php echo $form->error($model,'eventname'); ?>
</div>

<div class='row'>
    <?php echo $form->labelEx($model,'eventnamelist'); ?>
    <?php echo $form->dropDownList($model,'eventnamelist'); ?>
    <?php echo $form->error($model,'eventnamelist'); ?>
</div>

<div class='row buttons'>
    <?php //echo CHtml::submitButton($model->isNewRecord?'Create':'Save'); ?>
    <?php
    echo CHtml::ajaxSubmitButton('Save',
            CHtml::normalizeUrl(array("eventmaster/create")),
			array(
			    'success'=>'function(data){
                    $("#eventnamelist").html(data);
                }'
			),
            array(
                'id'=>'ajaxSubmitBtn',
                'name'=>'ajaxSubmitBtn'
            )
        );
    ?>   
</div>
```

## Ajax Button
## Ajax button can initiate AJAX requests.

```php
public static string ajaxButton(
    string $label, 
    mixed $url, 
    array $ajaxOptions=array ( ), 
    array $htmlOptions=array ( )
)
```
```
$lable - the button label
$url - the URL for the AJAX request. If empty, it is assumed to be the current URL.
$ajaxOptions - data, success, update, replace,etc..;
$htmlOptions - add HTML attributes here ex. name, id etc
```
## Sample code for ajaxButton 1:
```php
<?php
    echo CHtml::ajaxButton(
        // Lable of ajax button
        'Ajax Button Request',
        // Request function in controller of yii
        array('eventmaster/create'),
        //Parameter with ajax request
        array(
            'data'=>array('eventid'=>$model->id),  
            //update field after ajax response content 
            'update'=>'#update_selector',
        )
    );
?>
```
Sample code for ajaxButton 2:
```php
<?php
echo CHtml::ajaxButton(
        // Lable of ajax button
        'Ajax Button Request',
        // Request function in controller of yii
        array('eventmaster/create'),
        //Parameter with ajax request
        array(
            'data'=>array(
                'id'=>$model->year_id,
                'eventname'=>'js:$("#eventname").val()', 
                'eventtype'=>'js:$("#eventtype").val()', 
            ),
            'type'=>'POST', 
            //update field after ajax response content 
            'update'=>'#update_selector'
            //this is the update selector of yours $('#update_selector').load(url); 
        ) 
    ); 
?>
```
Sample code for ajaxButton 3:
```php
<?php
Yii::app()->clientScript->registerScript('callfunction',"
            function callfunction(){
                return 'welcome';
            }
");
?>

<?php
CHtml::ajaxButton(
        'Ajax Button Name',
        CController::createUrl(
                    "controller_name/action_name",
                    array("id"=>option_id)
            ),// Ajax Request Url 
        array(
            'type'=>'GET', // Data Request Type
            'data'=>'js:callfunction()', // Add Data 
            'success'=>"function(response)
            { // Response Function
                if(response!='' && response!=null)
                {
                    window.location.reload();
                }
            }"                
        ),            
        array('id'=>'ajax-button-id')// Set id for ajax button
    );
?>
```
## Ajax Link
## Ajax link that can initiate AJAX requests.

```php
public static string ajaxButton(
    string $text, 
    mixed $url, 
    array $ajaxOptions=array ( ), 
    array $htmlOptions=array ( )
)
```
```
$lable - the link text
$url - the URL for the AJAX request. If empty, it is assumed to be the current URL.
$ajaxOptions - data, success, update, replace,etc..;
$htmlOptions - add HTML attributes here ex. name, id etc
```

Sample code for ajaxlink: 
```php
<?php
echo CHtml::ajaxLink(
        "Request Link", 
        array("eventmaster/create"),
        array('id'=>'EVENT'),
    );
    
?>
```
## Ajax Support
>When you use ajax consider following functions and make ajax efficiently. It will be helpful

```php
<?php
echo CHtml::ajaxButton(
        // Lable of ajax button
        'Ajax Button Request',
        // Request function in controller of yii
        array('eventmaster/create'),
        //Parameter with ajax request
        array( // array start
            'data'=>array('eventid'=>$model->id),
            'beforeSend'=>'js:function(){
                alert("BEFORE SEND");
            }',

            'success'=>'js:function(data){
                alert("SUCCESS"+data);
            }',

            'complete'=>'js:function(){
                alert("COMPLETE");
            }',

            'error'=>'js:function(){
                alert("ERROR");  
            }',
        )// array end
);
```

## You can  use following methods to generate ajax link
```php
array('controller/action')
array('action') - defaultly it will take current controller
Yii::app()->createAbsoluteUrl(..) 
CHtml::normalizeUrl(array("controller/action"))
CController::createUrl("controller/action")
```