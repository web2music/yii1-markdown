## CHtml dropDownList Ajax Submit
I added the code to submit the yii form When we select the value from Chtml dropdownlist box.

```html
<div class="search-form" style="display:block">

    <div class="wide form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>

    <div class="row">
    <?php echo $form->labelEx($model,'type'); ?>

    <?php 
        $types=array("1"=>"Type1","2"=>"Type2");
        echo $form->dropDownList($model,'type',$types,
            array(
                'onchange'=>CHtml::ajax(array(
                    'success'=>"$('.search-form form').submit()",
                )),
            )
        ); ?>
    </div>
    </div>
</div>
```