# Yii ClientScript Css, Js

CClientScript manages JavaScript and CSS stylesheets for views.

- CoreScript
- register New Css, Js File
- Map Single Script
- Minimize Script File
- Disable Js, Css
- Include Js, Css Files Of Extensions
- register Style Code
- register Script code

### CoreScript
registerCoreScript method used to include the script files from core script library (From framework library) to application. So we can call the available files.

```php

Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerCoreScript('treeview');

```

### register New Css, Js File
Using registerCoreScript method, we can include the available files from framework. If we want to include the more file, registerCssFile method will helpful to add new files for application

```php

$cssFile=Yii::app()->baseUrl."/css/mystyle.css";
$jsFile=Yii::app()->baseUrl."/js/myscript.css";
$cs=Yii::app()->clientScript;
//$cs->registerCssFile( URL, POSITION);
$cs->registerCssFile($cssFile);
//$cs->registerScriptFile( URL, MEDIA);
$cs->registerScriptFile($jsFile);

```

> CSS MEDIA : print/handheld/screen
> JS  POSITION :

```php

CClientScript::POS_HEAD : the script is inserted in the head section right before the title element.
CClientScript::POS_BEGIN : the script is inserted at the beginning of the body section.
CClientScript::POS_END : the script is inserted at the end of the body section.
CClientScript::POS_LOAD : the script is inserted in the window.onload() function.
CClientScript::POS_READY : the script is inserted in the jQuery's ready function.
```

### Map Single Script

```php

Yii::app()->clientScript->scriptMap['jquery.js'] = "js/jquery1.9.1.js";
Yii::app()->clientScript->scriptMap['pager.css'] = "css/newpager.css";

```

### Minimize Script File
When we use more js file, It may take more times to load. Using scriptMap property of the clientScript application component, we can change this workflow. Just we have to merge and reduce the size of number(n js) of js file into one file. See below code, 'jquery.js,jquery.ajaxqueue.js,jquery.metadata.js' files are merged and mapped to'allscript.js' file. So When you call 'jquery.js' file, yii will automatically include the 'allscript.js' file url instead of 'jquery' url.


```php

$cs=Yii::app()->clientScript;
$cs->>scriptMap=array(
'jquery.js'=>'/js/allscript.js',
'jquery.ajaxqueue.js'=>'/js/allscript.js',
'jquery.metadata.js'=>'/js/allscript.js',
'pager.css'=>'/css/pager.css',
......
);

```


### Disable Js, Css
Instead of framework 'js' file, We can use another 'js' file. For example, Yii have the 1.7 version jquery. But we 1.9 version jquery. To change old version to new version, we can use scriptMap or disable the old version and load new version directly in theme file. To disable the framework js, use the below format

### Disable Many JS, CSS

```php

$cs=Yii::app()->clientScript;
$cs->scriptMap=array(
'jquery.js'=>false,
'jquery.ajaxqueue.js'=>false,
'jquery.metadata.js'=>false,
//*.js=>false
//disable all js
'pager.css'=>false,
......
);

```


### Disable One

> Yii::app()->clientScript->scriptMap['jquery.js'] = false;

It will prevent yii from including the file into application.
Include Js, Css Files Of Extensions

```php

<?php
$extensionAssetsJsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.thumbnail.js'));
$extensionAssetsCssUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.thumbnail.css'));
Yii::app()->clientScript->registerScriptFile($extensionAssetsJsUrl .'/thumbnail.js');
Yii::app()->clientScript->registerCssFile($extensionAssetsCssUrl.'/thumnailstyle.css');
?>

```

### register Style Code
registerCss function add the style text between <style>...</style> tags

```php

<?php
Yii::app()->clientScript->registerCss('style_css',<<<CSS
   a{
    color:red;
    }
CSS
);
?>

```

Output Css Style

```php

<style type="text/css">
/*<![CDATA[*/
   a{
    color:red;
    }
/*]]>*/
</style>

```

### register Script code
registerScript function add the script text between <script type="text/javascript">...</script> tags

> Yii::app()->clientScript->registerScript("welcome_js"," alert('welcome');",1/** position **/);

### POSITION:

```php

0 - END OF HEAD
1 - BEGIN OF BODY
2 - END OF BODY

```

Output Js Script

```php

<script type="text/javascript">
/*<![CDATA[*/

    alert('welcome');

/*]]>*/
</script>

```
