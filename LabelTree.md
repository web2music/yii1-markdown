# Label Tree

## Tab View

```php
<?php
$this->widget('CTabView', array(
    'tabs'=>array(
        'tab1'=>array(
            'title'=>'Basic Tree',
            'view'=>'label_view_1',
        ),
        'tab2'=>array(
            'title'=>'Parent Tag, Header, Controll',
            'view'=>'label_view_2',
        ),
        'tab3'=>array(
            'title'=>'Sourcecode',
            'view'=>'label_source',
        ),  
    ),
))?>
```

# Basic Tree
```php
<?php
    $data=array(
        '1'=>array('parentid'=>'','text'=>'One'),
        '2'=>array('parentid'=>'','text'=>'Two'),
        '3'=>array('parentid'=>'','text'=>'Three'),
        '11'=>array('parentid'=>'1','text'=>'One-One'),
        '12'=>array('parentid'=>'1','text'=>'One-Two'),        
    );
    Yii::import("application.extensions.AIOTree.*");
    $this->Widget('AIOTree',array(
        'data'=>$data,
        'type'=>'label',// or '', radio, checkbox
        'TreeId'=>'basic-tree',
        'controlId'=>'basic-tree-control',
        'parentShow'=>false,                
        'controlShow'=>false,
        'headerShow'=>false,
    ));
?>
```

# Parent Tag, Header, Control
```php
<?php    
    $data=array(
        '1'=>array('parentid'=>'','text'=>'One'),
        '2'=>array('parentid'=>'','text'=>'Two'),
        '3'=>array('parentid'=>'','text'=>'Three'),
        '11'=>array('parentid'=>'1','text'=>'One-One'),
        '12'=>array('parentid'=>'1','text'=>'One-Two'),        
    );
    Yii::import("application.extensions.AIOTree.*");
    $this->Widget('AIOTree',array(
        'data'=>$data,
        'type'=>'label',// or '', radio, checkbox
        'TreeId'=>'phctree',
        'TreeClass'=>'treeview-red',                
        'parentTag'=>'div',
        'parentId'=>'lparent2',
    ));
?>
```