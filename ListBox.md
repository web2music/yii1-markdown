## CHtml ListBox
> CHtml::listBox() function is used for creating ListBox in yii framework. This post will help you to understand about how to create ListBox in different method, How to assign value, how to get ListBox value in controller, ListBox value from database in yii framework.

* Syntax
* ListBox
* Static Values For ListBox
* ListBox Values From Database
* Listbox Group
* Listbox Values In Controller

Syntax

>public static string activeListBox(CModel $model, string $attribute, array $data, array $htmlOptions=array ( ))
[OR]
public static string listBox(string $name, mixed $select, array $data, array $htmlOptions=array ( ))

$model - the data model

$attribute - the attribute

$name - the input name

$select - the selected value(s). This can be either a string for single selection or an array for multiple selections.

$data - data for generating the list options (value=>display) . We can use CHtml::listData() to make this list array.

$htmlOptions- prompt, empty, encode and options

## ListBox
I used the country array for sample of listbox.

```html
$country = array('1' => 'India', '2' => 'Australia', '3' => 'America', '4' => 'England','5'=>'Russia');
```

activeListBox With CHtml
```php
<?php 
    $htmlOptions = array('size' => '5', 'multiple' => 'true','empty'=>'Select');
    echo CHtml::activeListBox($model,'country_id', $country, $htmlOptions);
?>
```
listBox With Form

```php  
<?php 
    $htmlOptions = array('size' => '5', 'multiple' => 'true','empty'=>'Select');
    echo $form->listBox($model,'country_id', $country, $htmlOptions);
?>
```
listBox With CHtml (Without Model Name)
```php
<?php 
    $htmlOptions = array('size' => '5', 'multiple' => 'true','empty'=>'Select');
    echo CHtml::listBox('Country_map[country_id]',array(), $country, $htmlOptions);
?>
```
Output
```html
<select id="Country_map_country_id" name="Country_map[country_id][]" multiple="multiple" size="5">
    <option value="1">India</option>
    <option value="2">Australia</option>
    <option value="3">America</option>
    <option value="4">England</option>
    <option value="5">Russia</option>
</select>
```
Static Values For ListBox
```php
<?php
    // assign the selected element on listbox
    $selected   = array(
        '1' => array('selected' => 'selected'),
        '3' => array('selected' => 'selected'),
    );
    $htmlOptions = array('size' => '5', 
        'multiple' => 'true', 
        'options' => $selected,
        'style'=>'',
        'empty'=>'Select'
    );
    echo $form->listBox($model,'country_id', $country, $htmlOptions);
?>
```
ListBox Values From Database
```php
<?php
    // assign the selected element and list element on listbox from database
    $country     = CHtml::listData(Country::model()->findAll(),'id','name');
    $selected_country    = CHtml::listData(Country_map::model()->findAll(),'id','name');
    $htmlOptions = array('size' => '5', 
       'multiple' => 'true', 
       'options' => $selected_country,
       'empty'=>'Select'
      );
    echo $form->listBox($model,'country_id', $country, $htmlOptions);
?>
```
Listbox Group
```php
<?php 
    $country = array('1' => 'Australia', 
                     '2' => 'America',
                     'Asian Countries'=>array(
                            '3'=>'India',
                            '4'=>'China',
                            '5'=>'More Countries'
                     ));
    //                     
    $htmlOptions = array('size' => '5', 'multiple' => 'true','empty'=>'Select Country');
    //
    echo CHtml::listBox('Country_map[country_id]',array(), $country, $htmlOptions);
?>
```
listbox-group-yii

Listbox Values In Controller
```php
$model->attributes=$_POST['Country_map'];
foreach($model->country_id as $value)
    echo $value;
// OR 1,2
$ID_STRING= implode(",",$model->country_id);
```    