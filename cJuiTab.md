# CJui Tabs
## Default CJuiTabs
StaticTabStaticTab With IDRender PartialAjaxTab
Content for tab 1

Dynamic Yii CJui Tabs Menu With Color
RedGreenBlue
Red Color

Yii CJui Tabs Mouse Over Event
RedGreenBlue
Red Color

>/** index.php ------------ **/

# <h1>Default CJuiTabs</h1>

```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs',array(
    'tabs'=>array(
        'StaticTab '=>'Content for tab 1',
        'StaticTab With ID'=>array('content'=>'Content for tab 2 With Id' , 'id'=>'tab2'),
        'Render Partial'=>array('id'=>'test-id','content'=>$this->renderPartial(
										'_renderpage',
										array('Values'=>'This Is My Renderpartial Page'),TRUE
										)),        
        // panel 3 contains the content rendered by a partial view
        'AjaxTab'=>array('ajax'=>$this->createUrl('ajax')),
    ),
    // additional javascript options for the tabs plugin
    'options'=>array(
        'collapsible'=>true,
    ),
    'id'=>'MyTab-Menu',
));
?>
```
```html
<br />
<h1>Dynamic Yii CJui Tabs Menu With Color </h1>
```
```php
<?php
$tablist=array("Red","Green","Blue");
foreach($tablist as $tabs){
    $css='';
		if($tabs=='Red'){$css='color:red;';}
		else if($tabs=='Green'){$css='color:green;';}
		else if($tabs=='Blue'){$css='color:blue';}	
		$tabarray["<span id='tab-$tabs' style='$css'>$tabs</span>"]="$tabs Color";
}
?>
<?php
$this->widget('zii.widgets.jui.CJuiTabs',array(
    'tabs'=>$tabarray,
    // additional javascript options for the accordion plugin
    'options' => array(
        'collapsible' => true,        
    ),
    'id'=>'MyTab-Menu1'
));
?>
```

<br />
<h1>Yii CJui Tabs Mouse Over Event </h1>

```php
<?php
$this->widget('zii.widgets.jui.CJuiTabs',array(
    'tabs'=>$tabarray,
    // additional javascript options for the accordion plugin
    'options' => array(         
        'event'=>'mouseover',
    ),
    'id'=>'MyTab-Menu-Mouse'
));
?>
```

>ajax.php
## /** ajax.php **/

```php
<?php
    echo "Ajax Rendered This Page";    
?>
```

>_renderpage.php
## /** _renderpage.php **/

```php
<?php
    echo $Values."<br/>";
    echo "Render Page<br/>";
?>
```