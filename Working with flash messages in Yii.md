# Working with flash messages in Yii  
>Posted on September 22nd, 2010 in PHP, Yii | No Comments ?
if you would like to inform the user, that his changes were successfully saved, you could add the following line to your Controller:

```php
Yii::app()->user->setFlash('success',"Data saved!");

```
Displaying the flash message in a view is done by

```php
if( Yii::app()->user->hasFlash('success') ) {
    echo Yii::app()->user->getFlash('success'); 
}

``` 

If you want the flash message to automatically fade out after a few seconds, you will have to add the following lines to your view:

```php
Yii::app()->clientScript->registerScript( 
			    'myHideEffect', 
			    '$("div[class^=flash-]").animate({opacity: 1.0}, 3000).fadeOut("slow");',
			    CClientScript::POS_READY 
			);

```