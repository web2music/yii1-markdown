## CHtml Checkbox List
>Using CHtml class we are creating checkBoxList in yii. This post will help you to understand about how to create checkBoxList in different method, How to assign value, how to get checkBoxList value in controller, checkBoxList value from database, checkBoxList validation in model and checkbox list style.

- checkBoxList
- checkBoxList Validation In Model
- checkBoxList Value In Controller
- Selected checkbox List Value From Database
- checkbox List Style
- checkBoxList

Using below code, we can create checkbox list in different way. And i added checkbox list value from database and assigned static array.

activeCheckBoxList With CHtml
CHtml activeCheckBoxList generates a checkbox list for a model attribute.

```php
<?php 
echo CHtml::activeCheckBoxList(
            $model,
            'name',
            array('1'=>'One','2'=>'Two')
        ); 
//OR 
$type_list=CHtml::listData(
            Types::model()->findAll(),
            'id',
            'type'
            );
echo CHtml::activeCheckBoxList($model,'name',$type_list); 
?>
```
checkBoxList With Form
```php
<?php 
echo $form->checkBoxList(
            $model,
            'name',
            array('1'=>'One','2'=>'Two')
        ); 
//OR
$type_list=CHtml::listData(
            Types::model()->findAll(),
            'id',
            'type'
        );
echo $form->checkBoxList($model,'name',$type_list); 
?>
```
checkBoxList With CHtml (Without Model Name)

```php
<?php 
//syntax:CHtml::checkBoxList(string name, default_select_by_array_keys,array_value());
<?php 
echo CHtml::checkBoxList(
            'Typelist[name]',
            $selected_Array=array(),
            array('1'=>'One','2'=>'Two')
        ); 
//OR
$type_list=CHtml::listData(
                Types::model()->findAll(),
			    'id',
			    'type'
            );
echo CHtml::checkBoxList(
            'Typelist[name]',
            $selected_Array=array(),
            $type_list
        );
?> 
?>
```
Output HTML
```html
<span id="Typelist_name">
    <input value="1" id="Typelist_name_0" type="checkbox" name="Typelist[name][]">
    <label for="Typelist_name_0">One</label>br<
    <input value="2" id="Typelist_name_1" type="checkbox" name="Typelist[name][]">
    <label for="Typelist_name_1">Two</label>
</span>
```
checkBoxList Validation In Model
Add the validation to checkBoxList value in model.

```php
<?php
.....
array('name', 'in','range'=>array(1,2)),
.....
?>
```

checkBoxList Value In Controller
```php
<?php
.........
$model->attributes=$_POST['Typelist'];
foreach($model->name as $checkbox_id)
{
    echo $checkbox_id;
}
.......
?>
```
Selected checkbox List Value From Database

```php
//already selected value
$selected_type_list=CHtml::listData(
                Typelist::model()->findAll(),
                'id',
                'type'
                );
//all type list
$type_list=CHtml::listData(
                Types::model()->findAll(),
                'id',
                'type'
                );
echo CHtml::checkBoxList(
                'typelist', 
                $selected_type_list,
                $type_list
                );
```
## checkbox List Style
Normally checkbox will display one by one. We can change this style using labelOptions. See below code

```php
<?php
echo $form->checkBoxList(
                $model,
                'type',
                array('1'=>'Admin','2'=>'User'),
                array(
                    'template'=>'{input}{label}',
                    'separator'=>'',
                    'labelOptions'=>array(
                        'style'=>'
                        padding-left:13px;
                        width: 60px;
                        float: left;'
                        ),
                    'style'=>'float:left;',
                )                              
            );
?>
```
```html
<div style="clear: both;"></div>
```