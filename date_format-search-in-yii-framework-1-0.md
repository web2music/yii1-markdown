# DATE_FORMAT Search In Yii Framework 1.0

>This tutorial will help you to find the result using `DATE_FORMAT()` function. 'DATE_FORMAT' function is used to convert the integer to date format value. I add the source code for this type of date search. In addCondition i used `DATE_FORMAT` function to filter the result.


```php
<?php
public function search()
{
    $criteria=new CDbCriteria;
    // condition for >= single date
    if(!empty($this->from_date) && empty($this->to_date)) {	       
        $this->from_date=date('Y-m-d',strtotime($this->from_date));
        $criteria->addCondition(
            "DATE_FORMAT(checkin_time, '%Y-%m-%d') >= '$this->from_date'"
        );
    }  
    // condition for <= single date
    elseif(!empty($this->to_date) && empty($this->from_date)) {
        $this->to_date=date('Y-m-d',strtotime($this->to_date));
        $criteria->addCondition(
            "DATE_FORMAT(checkin_time, '%Y-%m-%d') <= '$this->to_date'"
        );
    }
    // condition between two date
    elseif(!empty($this->from_date) && !empty($this->to_date)) {
        $this->from_date=date('Y-m-d',strtotime($this->from_date));
        $this->to_date=date('Y-m-d',strtotime($this->to_date));
        $criteria->addCondition(
            "DATE_FORMAT(checkin_time, '%Y-%m-%d') >='$this->from_date' 
            and 
            DATE_FORMAT(checkin_time, '%Y-%m-%d') <= '$this->to_date'"
        );
    }

    return new CActiveDataProvider($this, array(
        'criteria'=>$criteria,
    ));	
}
?>

```