# Yii Model Delete

>This tutorial will help you to delete the record from database table using model in yii framework. I added the code to delete one or more record using yii framework model.


## /** 1 **/
```php
$model=User::model()->findByPk($id);
if($model)
    $model->delete();

```
## /** 2 **/
```php
$model=User::model()->deleteAll();

```
## /** 3 **/
```php
$model=User::model()->deleteAll(array("condition"=>"userid='$id'"));

```
## /** 4 **/
```php
$mode=User::model()->deleteAll("status='A'");

```