# Yii CListview pager

>Yii Pager. I explained about "How to change yii pagination text". CGridview and CListview are the most used widgets in yii. It have the pagination number and text. Here i added source code to change the text of pagination header text, previous text, next text, first page text, last page text.

>This is the source code to change the yii default pager text for CListview. It is also common for CGridview widget.


```php
<?php
$this->widget('zii.widgets.CListView', array(
 'dataProvider'=>$dataProvider,
 'itemView'=>'_userlist',
    'pager'=>array('cssFile'=>'/css/mypager.css'),
    'summaryText'=>'',//'Result {start} - {end} of {count} results'
    'pager' => Array(
        'cssFile'=>Yii::app()->theme->baseUrl."/css/pagination.css",
        'header' => 'Go To Page',
        'prevPageLabel' => 'Previous',
        'nextPageLabel' => 'Next',
        'firstPageLabel'=>'First',
        'lastPageLabel'=>'Last'
    ),
)); 
?>

```