# How to display image in CDetailView Yii Framework 1.0

>This tutorial will help you to display the image in CDetailview widget of yii framework 1.0. We added a different funcitons in model.php to display the image with different methods like only image, image with link, image from database etc.

## View.php

```php
<?php 
$this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(	
        array(
            'name'=>'image',
            'type'=>'raw',
            'value'=>$model->image_tag(),            
        ),
        array(
            'name'=>'imagelink',
            'type'=>'raw',
            'value'=>$model->image_with_link(),
        ),
        array(
            'name'=>'image_database',
            'type'=>'raw',
            'value'=>$model->showphoto_from_database(),
        )
	),
)); ?>

```
## model.php
>Display Image Only

```php
public function image_tag(){
    return CHtml::image(Yii::app()->baseUrl."/images/Sub.png");
}

```

Display Image With Link
---

```php
public function image_with_link(){
    $html= CHtml::link(
        "<img src='".Yii::app()->baseUrl."/images/reports.png"."' />",
		Yii::app()->createAbsoluteUrl("payment/invoice",array("id"=>$this->order_id))
        );
    return $html;
}

```

Display Image From Database
---

```php
public function showphoto_from_database(){
    if($this->bitmap!=''){
        return CHtml::image("data:image/png;base64,".$photomodel->bitmap,
                'No Image',
                array('width'=>150,'height'=>100)
        );
    }else{
        $url=Yii::app()->baseUrl."/img/noimage.jpg";                
        return CHtml::image($url,'No Image');
    }
}

```
