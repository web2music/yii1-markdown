# Yii CGridview Button Configuration

In this article, I explained about Cgridview "CButtonColumn" of yii framework. Here i added new button and assign customized url to new button. We can change the url to "view","update","delete" button also.
```css
<?php $this->widget('zii.widgets.grid.CGridView', array(
 'id'=>'user-grid',
 'dataProvider'=>$model->search(),
 'columns'=>array(
                 'username',
                 'type',
                  array(
                  'class'=>'CButtonColumn',
                           'template'=>'{view}{update}{delete}{newbutton}',
                        'buttons'=>array(
                        'newbutton'=>array(
                               'label'=>'Click',
                               'url'=>'Yii::app()->createAbsoluteUrl('controller/action'))',
                               'visible'=>'$data["type"]=="admin"?false:true',
                               ),               
                         ),
                  ),
 ),
));

?>

```
