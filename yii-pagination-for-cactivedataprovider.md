# Yii Pagination For CActiveDataProvider
>This tutorial will helpful to understand about pagesize in cgridview or listview. Here i added the source code to show number of records per page.


```php
$criteria=new CDbCriteria;
if($isdesired!=''){
    $criteria->addCondition("status=1");
    $pagination=array('pageSize'=>8);
    $dataProvider=new CActiveDataProvider('Document',
        array('criteria'=>$criteria,'pagination'=>$pagination)
    );
    $this->render('documentviewindex', array('dataProvider'=>$dataProvider, ));
}

```