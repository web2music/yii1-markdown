## CHtml Radio Button List
>Using CHtml class we are creating radioButtonList in yii framework. This post will help you to understand about how to create radioButtonList in different method, How to assign value, how to get radioButtonList value in controller, radioButtonList value from database, radioButtonList validation in model and radion button list style in yii framework.

* radioButtonList
* radioButtonList Validation In Model
* radioButtonList Value In Controller
* Selected radiobutton List Value From Database
* radiobutton List Style

## radioButtonList
Using below code, we can create radiobutton list in different way. And i added radiobutton list value from database and assigned static array.

## activeRadioButtonList With CHtml
CHtml activeRadioButtonList generates a radiobutton list for a model attribute in yii framework.


```php
<?php 
echo CHtml::activeRadioButtonList($model,'name',array('1'=>'One','2'=>'Two')); 
//OR 
$type_list=CHtml::listData(Types::model()->findAll(),'id','type');
echo CHtml::activeRadioButtonList($model,'name',$type_list); 
?>
```
radioButtonList With Form

```php
<?php 
echo $form->radioButtonList($model,'name',array('1'=>'One','2'=>'Two')); 
//OR
$type_list=CHtml::listData(Types::model()->findAll(),'id','type');
echo $form->radioButtonList($model,'name',$type_list); 
?>
```
radioButtonList With CHtml (Without Model Name)

```php
<?php 
//syntax:CHtml::radioButtonList(string name, default_select_by_array_keys,array_value());

<?php 
echo CHtml::radioButtonList('Typelist[name]',$selected_Array=array(),array('1'=>'One','2'=>'Two')); 
//OR
$type_list=CHtml::listData(Types::model()->findAll(),'id','type');
echo CHtml::radioButtonList('Typelist[name]',$selected_Array=array(),$type_list);
?>
```
Output
```html
<span id="Typelist_name">
    <input value="1" id="Typelist_name_0" type="radio" name="Typelist[name][]">
    <label for="Typelist_name_0">One</label>br<
    <input value="2" id="Typelist_name_1" type="radio" name="Typelist[name][]">
    <label for="Typelist_name_1">Two</label>
</span>
```
## radioButtonList Validation In Model
Add the validation to radioButtonList value in model.

```php
<?php
.....
array('name', 'in','range'=>array(1,2)),
.....
?>
```
## radioButtonList Value In Controller
We can retrieve selected value one by one using foreach() in controller and also we can use imploed() directly on this radiobutton name.

```php
<?php
.........
$model->attributes=$_POST['Typelist'];
foreach($model->name as $radiobutton_id){
    echo $radiobutton_id;
}
.......
?>
```
## Selected radioButton List Value From Database
Get the radiobutton list value from database for selected value array and all value

```php
//selected value from db
$selected_type_list=CHtml::listData(Typelist::model()->findAll(),'id','type');
//all type list from db
$type_list=CHtml::listData(Types::model()->findAll(),'id','type');
echo CHtml::radioButtonList('typelist', $selected_type_list,$type_list);
```
## radioButton List Style
Normally radioButton will display one by one. We can change this style using labelOptions. See below code

```php
<?php
echo $form->radioButtonList($model,'type',
    array('1'=>'Admin','2'=>'User'),
    array(
        'template'=>'{input}{label}',
        'separator'=>'',
        'labelOptions'=>array(
            'style'=> '
                padding-left:13px;
                width: 60px;
                float: left;
            '),
            'style'=>'float:left;',
        )                              
    );
?>
```
```html
<div style="clear: both;"></div>
```