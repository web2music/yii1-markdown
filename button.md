## CHtml Button
CHtml is a static class and it is used to generate a button.

```php
public static string button(string $label='button', array $htmlOptions=array ( ))
```

> **CHtml** class used in **CFormButtonElement** to represents a form button element.
CFormButtonElement have the 8 types of button types. The types are htmlButton, button, submitButton, imageButton, resetButton and link button.

+ htmlButton: Type-button
+ htmlButton: Type-submit
+ htmlButton: Type-reset
+ button: Type-button
+ button: Type-submit
+ button: Type-reset
+ submitButton
+ imageButton
+ resetButton
+ linkButton
+ htmlButton: Type-button


```php 
<?php
echo CHtml::htmlButton('HTML BUTTON',array(
                "id"=>'chtmlbutton',
                "class"=>'chtmlbuttonclass')
                );
?>
```
Output

```html
<button type="button" name="yt0" 
                class="chtmlbuttonclass"
                id="chtmlbutton">HTML BUTTON</button>
```
### htmlButton: Type-submit

```php 
<?php
echo CHtml::htmlButton('HTML SUBMIT BUTTON',array(
                "id"=>'chtmlbutton',
                "class"=>'chtmlbuttonclass'));
?>
```
Output

```html
<button type="button" name="yt1" 
                class="chtmlbuttonclass" 
                id="chtmlbutton">HTML SUBMIT BUTTON</button>
```

### htmlButton: Type-reset

```php 
<?php
echo CHtml::htmlButton('HTML RESET BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass')
        );
?>
```
Output

```html
<button type="button" name="yt2" 
            class="chtmlbuttonclass" 
            id="chtmlbutton">HTML RESET BUTTON</button>
```            
### button: Type-button

```php 
<?php
echo CHtml::button('NORMAL BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass'));
?>
```
Output

```html
<input type="button" value="NORMAL BUTTON" 
            name="yt3" 
            class="chtmlbuttonclass" 
            id="chtmlbutton">
```

### button: Type-submit

```php 
<?php
echo CHtml::button('NORMAL SUBMIT BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass'));
?>
```
Output

```html
<input type="button" value="NORMAL SUBMIT BUTTON" 
            name="yt4" 
            class="chtmlbuttonclass" 
            id="chtmlbutton">
```
### button: Type-reset

```php 
<?php
echo CHtml::button('NORMAL RESET BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass'));
?>
```
Output

```html
<input type="button" value="NORMAL RESET BUTTON" 
            name="yt5" 
            class="chtmlbuttonclass" 
            id="chtmlbutton">
```
### submitButton

```php 
<?php
echo CHtml::submitButton('SUBMIT BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass'));
?>
```
Output

```html
<input type="submit" value="SUBMIT BUTTON" 
            name="yt6" 
            class="chtmlbuttonclass" 
            id="chtmlbutton">
```
### imageButton

```php 
<?php
echo CHtml::imageButton(
            Yii::app()->createAbsoluteUrl("/")."/../images/bg-menu-item-green.gif",
            array("style='border:1px solid;'")
        );
?>
```
Output

```html
<input type="image" value="submit" name="yt7" 
            src="http://127.0.0.1/blog/index.php/../images/bg-menu-item-green.gif" 
            style='border:1px solid;'>
```
### resetButton

```php 
<?php
echo CHtml::resetButton('RESET BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass')
            );
?>
```
Output

```html
<input type="reset" value="RESET BUTTON" 
            name="yt8" 
            class="chtmlbuttonclass" 
            id="chtmlbutton">
```
### linkButton

```php 
<?php
echo CHtml::linkButton('LINK BUTTON',array(
            "id"=>'chtmlbutton',
            "class"=>'chtmlbuttonclass'));
?>
```

Output

```html
<a href="#" class="chtmlbuttonclass" id="chtmlbutton">LINK BUTTON</a>
```