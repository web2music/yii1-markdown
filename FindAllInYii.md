# FindAll In Yii
Yii Framework. I list out all types of find, findall conditions in yii framework. It will very helpful for us.

```php
    Yii find() 
    Yii findAll()
    Yii findAll() Join
    Yii Sql command
    Yii find()
    find()
```

```php
<?php
    $model = User::model()->find();
    find() With Condition

<?php
    $model = User::model()->find('userid=1 AND status="A"');
    (OR)
    $model = User::model()->find(
                        'userid=:userId And status=:Status',
                        array(':userId'=>1,':status'=>'A')
                        );
    find() Width Criteria
```
```php
<?php
    $criteria = new CDbCriteria;
    $criteria->condition='userid=1 AND status="A"'; 
    $model = User::model()->find($criteria);
    (OR)
    $criteria=new CDbCriteria;     
    $criteria->condition='userid=:userId AND status=:Status';
    $criteria->params=array(':userId'=>10,':Status'=>'A');
    $model=User::model()->find($criteria);
    find() Max Id
```
```php
<?php
    $criteria=new CDbCriteria;
    $criteria->select='max(userid) as id';
    $model = User->model()->find($criteria);
    Yii findAll()
    findAll()
```

```php
<?php
    $model=User::model()->findAll();
    findAll() With Select

<?php
    $model=User::model()->findAll(array(
                        'select'=>'userid, username'
                    ));     
    findAll() With Conditon

<?php
    $model=User::model()->findAll(
                        array(
                            'select'=>'userid, username',
                            'condition'=>'status="A"'
                        ));
    findAll() With Conditon, Group

<?php
    $model=User::model()->findAll(
                        array(
                            'condition'=>'status="A"',
                            'group'=>'type'
                        ));  
    findAll() With Conditon, Order

<?php
    $model=User::model()->findAll(
                        array(
                            'select'=>'userid,username',
                            'condition'=>'status="A"',
                            'order'=>'username'
                        ));  
    findAll() With limit

<?php
    $model=User::model()->findAll(
                        array(
                            'condition'=>'status="A"',
                            'limit'=>'5'
                        ));  
    findAll() "with()" function

<?php
    $model=User::model()->with('login')
                        ->findAll(array(
                                'select'=>'t.userid,t.username,login.time',
                                'condition'=>'login.satus="L"'
                        ));
    findAllByPk Array value
```

```php
<php
    $usermodel=User::model()->findAllByPk($useridarray);
    $usermodel=User::model()->findAllByPk(array(2,3,10));
?>
```

## Yii findAll() Join
## findAll inner join

```php
<php
    $categorymodel=Category::model()->with(array(
                        'user'=>array(
                            'select'=>'categoryname',
                            'joinType'=>'INNER JOIN',
                            'condition'=>'user.categoryname="activeuser"',
                        ),
                    ))->findAll();
?>
```

## Yii Sql command
## Yii findAll() by Sql command

```php
public function findallbysql()
{
    $categorylist=Yii::app()->db->createCommand("
                SELECT category.categoryid as categoryid,category.categoryname as categoryname 
                FROM `documentindex` `t` 
                LEFT OUTER JOIN `tbl_category` `category` ON (`t`.`categoryid`=`category`.`categoryid`) 
                WHERE category.isactive=1 group by t.categoryid")
				->queryAll();
    $category_array["all"]="All";
    foreach($categorylist as $data)
    {
        $category_array[$data['categoryid']]=$data['categoryname'];
    }
    return $category_array;
}
