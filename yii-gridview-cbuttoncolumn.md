# Yii Gridview CButtonColumn

>CButtonColumn used to renders one or more buttons in gridview columns. Basically cgridview column have three buttons. They are `"view", "update", "delete"` button and it have corresponding actions. We can change it using CButtonColumn properties.


## Template
>Show the button using template string using below format

`'template'=>'{view}{update}{delete}',`

## We can add more buttons here.

## afterDelete
>After deleted the row, We can do some action using afterDelete.

## deleteButtonImageUrl
>Change the delete button image url using deleteButtonImageUrl

## deleteButtonLabel
>Change the delete button label using deleteButtonLabel

## deleteButtonOptions
>Change the delete button html properties like style, class, id etc using deleteButtonOptions.

## deleteConfirmation
>Change the delete confirmation message using deleteConfirmation

## footer
>Add footer value to cbuttoncolumn

## footerHtmlOptions
>Change the footer html properties like style, class, id etc using footerHtmlOptions.

## header
>Add header value to cbuttoncolumn

## headerHtmlOptions
>Change the header html properties like style, class, id etc using headerHtmlOptions.

## htmlOptions
>Change the html properties of cbutton column like style, class, id etc using htmlOptions.

## updateButtonImageUrl
>Change the update button image url using updateButtonImageUrl

## updateButtonLabel
>Change the update button label using updateButtonLabel

## updateButtonOptions
>Change the update button html properties like style, class, id etc using updateButtonOptions.

## viewButtonImageUrl
>Change the view button image url using viewButtonImageUrl

## viewButtonLabel
>Change the view button label using viewButtonLabel

## viewButtonOptions
>Change the view button html properties like style, class, id etc using viewButtonOptions.


## Sample Code

```php
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'countrymaster-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        'countryid',
        'countryname',
        array(
            'class'=>'CButtonColumn',
            //template details
            'template'=>'{view}{update}{delete}',
            //button details
            'buttons'=>array(),
            //delete details
            'afterDelete'=>'function(link,success,data){alert("success");}',
            'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/image/delete.png',
            'deleteButtonLabel'=>'Delete Button',
            'deleteButtonOptions'=>array(
                'style'=>'width:50px;',
                'class'=>'delete_class',
                'id'=>'delete_id',
            ),
            'deleteConfirmation'=>'Are you sure?',
            'filterHtmlOptions'=>array(),
            //footer details
            'footer'=>'Button Footer',
            'footerHtmlOptions'=>array(
                'style'=>'color:green;',
            ),
            //header details                             
            'header'=>'String Header',
            'headerHtmlOptions'=>array(
                'style'=>'color:red;',
            ),
            'htmlOptions'=>array(),
            'id'=>'button_column_id',
            //update details
            'updateButtonImageUrl'=>Yii::app()->baseUrl.'/image/update.png',
            'updateButtonLabel'=>'Button Header',
            'updateButtonOptions'=>array(
                'style'=>'width:50px;',
                'class'=>'update_class',
                'id'=>'update_id',
            ),
            //view  details                                    
            'viewButtonImageUrl'=>Yii::app()->baseUrl.'/image/view.png',
            'viewButtonLabel'=>'View',
            'viewButtonOptions'=>array(
                'style'=>'width:50px;',
                'class'=>'view_class',
                'id'=>'view_id',
            ),
            'visible'=>true,//or false
        ),
    ),
    'itemsCssClass'=>'Grid_View_Table_CSS_Class',
)); ?>

```
More Details

Gridview Button Configuration