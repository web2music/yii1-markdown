# How to change content of a cell in Grid View to a link  
> Posted on October 4th, 2010 in PHP, Yii | No Comments ?
If you want to link content of cell in Grid View to some page e.g In orders list, if you want customer’s name to be link that takes admin to customer’s detail page, change the columns of CGrid View as

```php
'columns' => array(
    'id',
    'user_id'=> array(
        'type'=>'html',
        'value' => 'CHtml::link( $data->user->name,
                       Yii::app()->createUrl( "admin/view", array("id"=>$data->user_id, "m"=>"users") ),
                       array("class"=>"facebox", "title"=>"View Customer Details") )',
        'header' => 'Customer'
    ),
    'product'=> array(
        'type'=>'html',
        'value' => 'CHtml::link( $data->product->name,
                       Yii::app()->createUrl( "admin/manage",
                             array( "Orders[product_id]"=>$data->product_id, "m"=>"orders" ) ),
                       array( "title"=>"View All Orders for ".$data->product->name ) )',
        'header' => 'Product'
    ),
    array(
        'class'=>'CButtonColumn',
    ),
)

```