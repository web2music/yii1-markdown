## Yii - Run cron command from controller

```php
class SystemController extends CController
{
  public function actionTestCron()
  {
    $cron5min = new PisendCommand('_', '_');
    // Our command expects array of params, so we pass desired argument in array
    $cron5min->run([9]);
  }
}
```

## Yii - add create_time, update_time fields to model

### Method 1

Do you want to track create time and update time for each record? Use Yii `behaviors`:

```php
class MortgageClaim extends CActiveRecord
{
    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
            ]
        ];
    }
}
```

Ensure that you have fields `create_time`, `update_time`. You can create them via migration:

```php
// ...
$this->createTable(
    'mortgage_claims',
    [
        // ...
        'create_time' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
        'update_time' => 'timestamp NOT NULL'
    ]
);
```

**Important:** In some cases you could get an error: 

```
include(CTimestampBehavior.php): failed to open stream: No such file or
directory (/var/www/html/vendor/yiisoft/yii/framework/YiiBase.php:427)
Stack trace:
#0 unknown(0): spl_autoload_call()
#1 /var/www/html/vendor/yiisoft/yii/framework/caching/CCache.php(108):
unserialize()
```

To fix error add this at `config/main.php`:

```php
'import' => array(
  // ...
	'zii.behaviors.CTimestampBehavior',		
),
```

### Method 2

Because of error in Method 1 we can implement desired behaviour manually:

```php
class Product {
    protected function beforeSave()
    {
        if (!parent::beforeSave()) {
	    return false;
	}
	
	if ($this->isNewRecord) {
	    $this->create_time = date('Y-m-d H:i:s');
	    $this->create_ip   = Yii::app()->request->userHostAddress;
	}
	$this->update_time = date('Y-m-d H:i:s');
	$this->update_ip   = Yii::app()->request->userHostAddress;
	
	return true;
    }
}
```

### Method 3

You can delegate this task to database. If you have MySQL >= 5.6.5, create fields with migration:

```php
public function up() {
    $this->createTable(
        'claims_processed',
        [
	    'id' => 'pk',
	    // ...
	    'create_time' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
	    'update_time' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]
    );
}
```

**Important:** Be careful! If you use this *method 3* you have to mention names of attributes that you want to update:

```php
$claim = Claim::model()->findByAttributes(['claim_id' => 123]);
$claim->status = 'SUCCESSFUL';
$claim->status_info = 'Processed';

// Won't work! This will NOT update `update_time` field
$claim->save();

// OK. This will update `update_time` field
$claim->save(true, ['status', 'status_info']);
```

## Log SQL query to browser

If you want to see all SQL queries in web browser, add this code to config file:

```php
// ...
'components' => [
    'log' => [
        'class'  => 'CLogRouter',
        'routes' => [
	    ['class' => 'CWebLogRoute'],
        ]
    ]
]
```