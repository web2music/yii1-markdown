# CTabView

## Sample
DirectContent View Content Link To Other Site
Content was placed directly..

## SourceCode

```php
<?php
$this->widget('CTabView', array(
    'tabs'=>array(
        'tab1'=>array(
            'title'=>'DirectContent',
            'content'=>'Content was placed directly..',
        ),
        'tab2'=>array(
            'title'=>'View Content',
            'view'=>'view1',
            //'data'=>array('model'=>$model),
        ),
        'tab3'=>array(
            'title'=>'Link To Other Site',
            'url'=>'http://www.bsourcecode.com/',
        ),
    ),
    'activeTab'=>'tab2',
    'cssFile'=>Yii::app()->baseUrl.'/css/jquery.yiitab.css',
    'htmlOptions'=>array(
            'style'=>'',
            ),
    'id'=>'Tab-Id'    
));
?>
```
