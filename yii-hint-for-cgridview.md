# Yii hint for CGridview

We can change the cgridview CButtonColumn visible status based on condition. Here i explained to show and hide the action buttons. using "visible" tag We can do this. To show the button set 'true' to visible and to hide the button set 'false' to visible.
```php
 array(
            'class'=>'CButtonColumn',
            'header' => 'List of Actions',
            'template'=>'{view}{update}{delete}{User}',
            'buttons'=>array(
                'User'=>array(
                    'lable'=>"User",
                    'url'=>'',
                    'visible'=>'$data["status"]==0?true:false',
                 ),
                'update'=>array(
                    'visible'=>'$data["status"]==0?true:false',
                 ),
                'delete'=>array(
                    'visible'=>'$data["status"]==0?true:false',
                 ),
                
            ),
 ),

 ```
