
# CJui Accordion
* Yii Default Accordion
* Yii Accordion Auto Height True/False
* Yii Accordion Icon
* Dynamic Yii Accordion Menu With Color


## /** View.php ------------ **/
```html
<h1>Yii Default Accordion</h1>
```

```php
<?php
$this->widget('zii.widgets.jui.CJuiAccordion',array(
    'panels'=>array(
        'panel 1'=>'content for panel 1',
        'panel 2'=>'content for panel 2',
        // panel 3 contains the content rendered by a partial view
        'panel 3'=>$this->renderPartial('_renderpage',null,true),
    ),
    // additional javascript options for the accordion plugin
    'options'=>array(
        'animated'=>'bounceslide',
        'style'=>array('minHeight'=>'100'),
    ),
    
));
?>
```
```html
<br />
<br />
<hr />
<br />
<h1>Yii Accordion Auto Height True/False</h1>
```
```php
<?php
$this->widget('zii.widgets.jui.CJuiAccordion',array(
    'panels'=>array(
        'panel 1'=>'content for panel 1',
        'panel 2'=>'content for panel 2',
        // panel 3 contains the content rendered by a partial view
        'panel 3'=>$this->renderPartial('_renderpage',null,true),
    ),
    // additional javascript options for the accordion plugin
    'options'=>array(
        'collapsible'=> true,
        'animated'=>'bounceslide',
        'autoHeight'=>false,
        'active'=>2,
    ),
));
?>
```

```html
<br />
<br />
<hr />
<br />
<h1>Yii Accordion Icon </h1>
```
```php
<?php
$this->widget('zii.widgets.jui.CJuiAccordion',array(
    'panels'=>array(
        'panel 1'=>'content for panel 1',
        'panel 2'=>'content for panel 2',
        // panel 3 contains the content rendered by a partial view
        'panel 3'=>$this->renderPartial('_renderpage',null,true),
    ),
    // additional javascript options for the accordion plugin
    'options'=>array(
        'animated'=>'bounceslide',        
        'icons'=>array(
            "header"=>"ui-icon-plus",//ui-icon-circle-arrow-e
             "headerSelected"=>"ui-icon-circle-arrow-s",//ui-icon-circle-arrow-s, ui-icon-minus
        ),
    ),
));
?>
```
```html
<br />
<br />
<hr />
<br />
<h1>Dynamic Yii Accordion Menu With Color </h1>
```
```php
<?php
$tablist=array("Red","Green","Blue");
foreach($tablist as $tabs){
    $css='';
		if($tabs=='Red'){$css='color:red;';}
		else if($tabs=='Green'){$css='color:green;';}
		else if($tabs=='Blue'){$css='color:blue';}	
		$tabarray["<span id='tab-$tabs' style='$css'>$tabs</span>"]="$tabs Color";
}
?>
<?php
$this->widget('zii.widgets.jui.CJuiAccordion',array(
    'panels'=>$tabarray,
    // additional javascript options for the accordion plugin
    'options'=>array(
        'animated'=>'bounceslide',        
        'icons'=>array(
            "header"=>"ui-icon-plus",//ui-icon-circle-arrow-e
             "headerSelected"=>"ui-icon-circle-arrow-s",//ui-icon-circle-arrow-s, ui-icon-minus
        ),
    ),
));
?>
```

## _renderpager.php
## /** _renderpager.php **/
```php
<?php
    echo "Render Page1<br/>";
    echo "Render Page2<br/>";
    echo "Render Page3<br/>";
    echo "Render Page4<br/>";
?>
```
