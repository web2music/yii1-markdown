# CGridview Auto Update With CJuiDialog

This tutorial will help you to update the cgridview with out page reload When you use the CJuiDialog Form to update the record. Some time We will do this type of concept. We have to show the listing and update action in cgridview widget page to avoid the new page for updating the record or some technical concept based on project. Here I used iframe concept with cjuidialog to update the record.

```php
<?php
Yii::app()->clientScript->registerScript('search', "

    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('subscriptionclaims-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>
```

## Manage Subscriptions

```php

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'subscriptionclaims-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(    
        'subscription_transaction_type_id',                    
        'amount',
        'credits',
        array(
            'class'=>'CButtonColumn',
            'header'=>'Actions',
            'template'=>'{update}',
            'buttons'=>array(                
                'update'=>array(
                    'url'=>'Yii::app()->createAbsoluteUrl("admin/update",array("id"=>$data->order_id))',
                    /**CJuiDialog Update Action Using IFrame**/
                    'click'=>'function(){
                        $("#grid-frame").attr("src",$(this).attr("href")); 
                        $("#grid-dialog").dialog("open");  
                        return false;
                    }',       
                ),
            ),            
        ),
    ),
)); ?>
```


```php
<?php
/** Dialog Widget Start With IFrame **/
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'grid-dialog',
    'options'=>array(
        'title'=>'Detail view',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>800,
        'close'=>'js:function(){ 
            /** We want this code to update the cgridview **/
            $("#grid-frame").attr("src","");
            $.fn.yiiGridView.update("subscriptionclaims-grid", {
                data: $(this).serialize()
            });            
        }',
    ),
));
?>
```

```html
<iframe id="grid-frame" width="100%" height="100%"></iframe>
```

```php
<?php
$this->endWidget();
/** Dialog Widget End **/
?>
```