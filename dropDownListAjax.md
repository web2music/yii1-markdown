## CHtml dropDownList Ajax
I added the code for yii dropdownlist to the following actions like Dropdownlist ajax, Ajax response etc


* CHtml dropDownList: Ajax
* CHtml dropDownList: Ajax Response(Ex1)
* CHtml dropDownList: Ajax Response(Ex2)
* CHtml dropDownList: Ajax Response(Ex3)
* CHtml dropDownList: Ajax

```php
<?php        
/** CHtml dropDownList Ajax**/          
    $type=array("1"=>"Type 1","2"=>"Type 2");
    echo $form->dropdownlist(
                $model,
                'type',
                $type,
                array(
                    'empty'=>'Select Type',
                    'ajax'=>array(
                        'type'=>'POST',
                        'url'=>CController::createUrl('user/type'),
                        'data'=>array('typeid'=>'js:this.value'),
                    ),
                )
        );

?>
```

## CHtml dropDownList: Ajax Response(Ex1)

```php
<?php
/** Update After Get Response **/          
$type=array("1"=>"Type 1","2"=>"Type 2");
echo $form->dropdownlist(
                $model,
                'type',
                $type,
                array(
                    'empty'=>'Select Type ',
                    'ajax'=>array(
                        'type'=>'POST',
                        'url'=>CController::createUrl('user/type'),
                        'update'=>'#Update-id',
                        'data'=>array('typeid'=>'js:this.value'),
                    ),
                )
        );       
?>
```
## CHtml dropDownList: Ajax Response(Ex2)

```php                       
<?php
    $ajax=array();
    $ajax['type']='POST';
    $ajax['url']= $this->createUrl("user/type");
    $ajax['update']='#Update-id';
    $ajax['data']="js:'parentname='+jQuery(this).val()";    
    $value['empty']='Select Type';
    $value['ajax']=$ajax;                                                                 
?>
```
```html
<div class="row">
    <?php echo $form->label($model,'type'); ?>
    <?php echo $form->dropDownList($model,'type',$type,$value); ?>
</div>
```

## CHtml dropDownList: Ajax Response(Ex3)

```php
<?php
/** Update After Get Response **/          
$type=array("1"=>"Type 1","2"=>"Type 2");
echo $form->dropdownlist(
                $model,
                'type',
                $type,
                array(
                    'empty'=>'Select Type',
                    'ajax'=>array(
                        'type'=>'POST',
                        'url'=>CController::createUrl('user/type'),
                        //'update'=>'#Update-id',
                        'update'=>'#'.CHtml::activeId($model, 'type')
                        'data'=>array('typeid'=>'js:this.value'),
                    ),
                )
        );       
?>
```