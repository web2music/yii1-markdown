# CStarRating
## Star Rating

```php
<?php
    $this->widget('CStarRating',array('name'=>'rating'));
?>
```

## Star Rating: Properties
* Default Value :4
* Minimum Rating :1
* Maximum Rating :10
* Star Count :10

```php
<?php
    $this->widget('CStarRating',array(
            'name'=>'rating_1',
            'value'=>'4',
            'minRating'=>1,
            'maxRating'=>10,
            'starCount'=>10,
            ));
?>
```



## Star Rating: Read Only
```php
<?php
$this->widget('CStarRating',array(
            'name'=>'rating_2',
            'value'=>'3',
            'readOnly'=>true,
            ));
?>
```

## Star Rating: Ajax
```php
<?php
    $this->widget('CStarRating',array(
        'name'=>'star_rating_ajax',
        'callback'=>'
            function(){
                $.ajax({
                    type: "POST",
                    url: "'.Yii::app()->createUrl('cstarrating/ajax').'",
                    data: "star_rating=" + $(this).val(),
                    success: function(data){
                                $("#mystar_voting").html(data);
                        }})}'
  ));
    echo "<br/>";
    echo "<div id='mystar_voting'></div>";
?>
```

## CStarRating: Titles

```php
<?php
    $this->widget('CStarRating',array(
            'name'=>'rating_4',
            'value'=>'3',
            'minRating'=>1,
            'maxRating'=>5,
            'titles'=>array(
                '1'=>'Normal',
                '2'=>'Average',
                '3'=>'OK',
                '4'=>'Good',
                '5'=>'Excellent'
            ),
            ));
?>
```