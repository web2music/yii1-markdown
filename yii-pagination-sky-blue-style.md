# Yii Pagination Sky Blue Style
Yii Pagination Style. Here i added one pagination style. see the style below

>pagination-skyblue



# Pager Css
>Disable Default CLinkPager
Pager Css
Save below sourcecode as pager.css under "css" folder of main directory. Then we have to call this file in "main.php" of theme file.

## pager.css

```css
ul.yiiPager li
{
    display:inline;
}

ul.yiiPager a:link,
ul.yiiPager a:visited
{
    border:solid 1px #3ABFEC;
    font-weight:bold;
    color:#3ABFEC;
    padding:1px 6px;
    text-decoration:none;
    padding: 3px 9px;    
}

ul.yiiPager .page a
{
    font-weight:normal;
}

ul.yiiPager a:hover
{
    border:solid 1px #0e509e;
}

ul.yiiPager .selected a
{
    background:#3ABFEC;
    color:#FFFFFF;
    font-weight:bold;
}

ul.yiiPager .hidden a
{
    border:solid 1px #DEDEDE;
    color:#888888;
}

/**
 * Hide first and last buttons by default.
 */
ul.yiiPager .first,
ul.yiiPager .last
{
    display:none;
}

```

## Disable Default CLinkPager
>After completed about process, we have to disable default pager css file. We have to add the following code into config/main.php file. After this, you can see the new pagination style.

```php
............
'components'=>array(
'widgetFactory'=>array(
    'widgets'=>array(
        'CLinkPager'=>array(
            'maxButtonCount'=>5,
            'cssFile'=>false,
            'header'=>false,
        ),
    ),
),    
.................

```